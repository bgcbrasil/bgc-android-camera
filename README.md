# bgc-android-camera  

An Android library to provide:

* Camera Preview (Front & Back)
* Face Detection
* Document Detection
* Liveness Detection
* Integration with BGC services
  * Face Match
  * OCR
  * Storage for Faces and Documents

![build](https://img.shields.io/bitbucket/pipelines/bgcbrasil/bgc-android-camera/master)
![pr](https://img.shields.io/bitbucket/pr/bgcbrasil/bgc-android-camera)
# Table of Contents

- [bgc-android-camera](#bgc-android-camera)
- [Table of Contents](#table-of-contents)
- [Installation](#installation)
- [Usage](#usage)
  - [FaceView](#faceview)
    - [Start saving faces](#start-saving-faces)
    - [Start matching faces](#start-matching-faces)
    - [Face Events](#face-events)
  - [Document View](#document-view)
    - [Start analyzing documents](#start-analyzing-documents)
  - [LivenessView](#livenessview)
    - [Start liveness check](#start-liveness-check)
- [API](#api)
  - [Methods](#methods)
  - [Events](#events)
    - [Candidate Face Result](#candidate-face-result)
    - [Candidate FaceMatch Result](#candidate-facematch-result)
    - [OCR CNH Result](#ocr-cnh-result)
    - [OCR RG Result](#ocr-rg-result)
    - [Errors](#errors)
- [Services](#services)
  - [BGC Image Recognition](#bgc-image-recognition)
  - [BGC OCR](#bgc-ocr)
  
# Installation

Add the JitPack repository to your root `build.gradle` at the end of repositories

```groovy
allprojects {
 repositories {
 ..
 maven { url 'https://jitpack.io' }
 }
}
```

Add the dependency

```groovy
dependencies {
 implementation 'org.bitbucket.bgcbrasil:bgc-android-camera:master-SNAPSHOT'
}
```

# Usage

The functionalities that the `bgc-android-camera` provides is divided between tree views to optimize usage. 

You can use `FaceView`, `DocumentView` and `LivenessView`, bellow are basic examples using kotlin, for more details, see the [API](#api) sections
## FaceView

> **_NOTE:_**  Do not forget request camera permission.

Import FaceView in your layout XML:

```kotlin
<bgc.brasil.camera.FaceView
  android:id="@+id/face_view"
  android:layout_width="match_parent"
  android:layout_height="match_parent" />
```

And inside your code:

```kotlin
var faceView: FaceView
this.faceView = face_view
```

### Start saving faces

With face view initialized, we can start capturing and matching faces.
```kotlin
this.faceView.startPreview()
```

If you want to save the captured face on BGC image recognition service, you must enable the createFaceOnDetect variable and set token before starting face capture:

```kotlin
this.faceView.setToken("YOUR_BGC_TOKEN")
this.faceView.setCreateFaceOnDetect(true)
```

This will call the service when the plugin detects a good face image and stoping future faces captures after the image is saved securely in our database.

You can also set the listen to camera events when a face is created in our database. This events returns an identifier (used later for face match) and a result containing parameters of bgc face create analysis.

```kotlin
this.faceView.setCameraEvents(object: CameraEvents() {
    override fun onFaceCreated(id: String?, result: CandidateFaceResult?) {
        // YOUR CODE
    }
})
```
### Start matching faces
The Face Match complements Face Creation, with this functionality you can validate if a face matches with a previosly created face. 

```kotlin
this.cameraView.setToken("YOUR_BGC_TOKEN")
this.cameraView.setFaceMatchOnDetect(true, "43531bcf-ee0b-4a06-bc9a-ff3f1f60dbe7")
```

When a face matches (or not) the response will trigger these events with and identifier and a result containing parameters of bgc face match analysis
```kotlin
this.faceView.setCameraEvents(object: CameraEvents() {
    override fun onFaceMatch(
        id: String?,
        result: CandidateFaceMatchResult?
    ) { 
        // YOUR CODE 
    }

    override fun onFaceUnmatch(
        id: String?,
        result: CandidateFaceMatchResult?
    ) { 
        // YOUR CODE 
    }
})
```

### Face Events

These are corresponding camera events available to FaceView. All events are optional.

```kotlin
this.cameraView.setCameraEventListener(object : CameraEventListener {
    override fun onImageCaptured(
        type: String,
        count: Int,
        total: Int,
        imagePath: String,
        inferences: ArrayList<android.util.Pair<String, FloatArray>>,
    ) {
        // YOUR CODE
    }

    override fun onEndCapture()  { 
        // YOUR CODE 
    }
    override fun onError(
        error: String
    )  { 
        // YOUR CODE 
    }
    override fun onMessage(
        message: String
    )  { 
        // YOUR CODE 
    }
    override fun onPermissionDenied()  { 
        // YOUR CODE 
    }
    override fun onSuccessDialogClose()  { 
        // YOUR CODE 
    }
    override fun onFaceDetected(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        leftEyeOpenProbability: Float?,
        rightEyeOpenProbability: Float?,
        smilingProbability: Float?,
        headEulerAngleX: Float,
        headEulerAngleY: Float,
        headEulerAngleZ: Float
    ) { 
        // YOUR CODE 
    }

    override fun onFaceUndetected()  { 
        // YOUR CODE 
    }
    
    override fun onFaceCreated(
        id: String?,
        result: CandidateFaceResult?
    ) { 
        // YOUR CODE 
    }
    
    override fun onFaceCreationFail(
        error: String?
    ) { 
        // YOUR CODE 
    }

    override fun onFaceMatch(
        id: String?,
        result: CandidateFaceMatchResult?
    ) { 
        // YOUR CODE 
    }

    override fun onFaceUnmatch(
        id: String?,
        result: CandidateFaceMatchResult?
    ) { 
        // YOUR CODE 
    }

    override fun onFaceMatchFail(
        error: String?,
    ) { 
        // YOUR CODE 
    }
})
```

## Document View

> **_NOTE:_**  Do not forget to request camera permission.

Import DocumentView in your layout XML:

```kotlin
<bgc.brasil.camera.DocumentView
  android:id="@+id/document_view"
  android:layout_width="match_parent"
  android:layout_height="match_parent" />
```

And inside your code:

```kotlin
var documentView: DocumentView
this.documentView = document_view

```

With document view initialized, we can start capturing documents.
```kotlin
this.documentView.startPreview()
```

### Start analyzing documents

To use BGC OCR, you need to configure the view with your token and enable the OCR functionality:

```kotlin
this.documentView.setToken("YOUR_BGC_TOKEN")
this.documentView.setDocumentOCR(true, 'cnh') // it's necessary to pass the document type
```

When the camera detects a document image, it will enable the capture button. Our OCR will receive the document and return all infos to these events.
```kotlin
this.documentView.setCameraEvents(object: CameraEvents() {
    override fun onCNHCreated(id: String?, result: OCRCNHResult?) {
        // YOUR CODE
    }

    override fun onCNHCreationFail(error: String?) {
        // YOUR CODE
    }

    override fun onRGCreated(id: String?, result: OCRRGResult?) {
        // YOUR CODE
    }

    override fun onRGCreationFail(error: String?) {
       // YOUR CODE
    }
})
```

These are corresponding camera events available to DocumentView. All events are optional.

```kotlin
this.cameraView.setCameraEventListener(object : CameraEventListener {
    override fun onImageCaptured(
        type: String,
        count: Int,
        total: Int,
        imagePath: String,
        inferences: ArrayList<android.util.Pair<String, FloatArray>>,
    ) {
        // YOUR CODE
    }

    override fun onEndCapture() {
        // YOUR CODE
    }

    override fun onError(
        error: String
    ) {
        // YOUR CODE
    }

    override fun onMessage(
        message: String
    ) {
        // YOUR CODE
    }

    override fun onPermissionDenied() {
        // YOUR CODE
    }

    override fun onSuccessDialogClose() {
        // YOUR CODE
    }

    override fun onCNHCreated(
        id: String?,
        result: OCRCNHResult?
    ) {
        // YOUR CODE
    }
    
    override fun onCNHCreationFail(
        error: String?
    ) {
        // YOUR CODE
    }

    override fun onRGCreated(
        id: String?,
        result: OCRRGResult?
    ) {
        // YOUR CODE
    }

    override fun onRGCreationFail(
        error: String?
    ) {
        // YOUR CODE
    }

})
```

## LivenessView

> **_NOTE:_**  Do not forget to request camera permission.

Import LivenessView in your layout XML:

```kotlin
<bgc.brasil.camera.LivenessView
  android:id="@+id/liveness_view"
  android:layout_width="match_parent"
  android:layout_height="match_parent" />
```

And inside your code:

```kotlin
var livenessView: LivenessView
this.livenessView = liveness_view

```

With liveness view initialized, we can start liveness preview.
```kotlin
this.livenessView.startPreview()
```

### Start liveness check

To use the liveness detection you need to activate it, this time you don't need BGC TOKEN.
```kotlin
livenessView.setCameraLens("front") // back or front
livenessView.setSteps(arrayOf("box", "lookUp", "box", "lookDown", "smile")) // array with all checks
```

This event will be called when the challenges have been completed.
```kotlin
this.documentView.setCameraEvents(object: CameraEvents() {
    override fun onLiveness(liveness: Boolean) {
        // YOUR CODE
    }
})
```


These are corresponding camera events available to LivenessView. All events are optional

```kotlin
this.cameraView.setCameraEventListener(object : CameraEventListener {
    override fun onImageCaptured(
        type: String,
        count: Int,
        total: Int,
        imagePath: String,
        inferences: ArrayList<android.util.Pair<String, FloatArray>>,
    ) {
        // YOUR CODE
    }

    override fun onEndCapture() {
        // YOUR CODE
    }

    override fun onError(
        error: String
    ) {
        // YOUR CODE
    }

    override fun onMessage(
        message: String
    ) {
        // YOUR CODE
    }

    override fun onPermissionDenied() {
        // YOUR CODE
    }

    override fun onSuccessDialogClose() {
        // YOUR CODE
    }

    override fun onFaceDetected(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        leftEyeOpenProbability: Float?,
        rightEyeOpenProbability: Float?,
        smilingProbability: Float?,
        headEulerAngleX: Float,
        headEulerAngleY: Float,
        headEulerAngleZ: Float
    ) {
        // YOUR CODE
    }

    override fun onFaceUndetected() {
        // YOUR CODE
    }

    override fun onLiveness(
        liveness: Boolean
    ) {
        // YOUR CODE
    }

})
```
# API

## Methods

| Function              | Parameters                                    | Valid values                                                    | Return Type | Availability | Description                                                                                                                                     |
|-----------------------|-----------------------------------------------|-----------------------------------------------------------------|-------------|--------------|-------------------------------------------------------------------------------------------------------------------------------------------------|
| startPreview          | -                                             | -                                                               | void        | All          | Start camera preview if has permission.                                                                                                         |
| stopCapture           | -                                             | -                                                               | void        | All          | Stop any type of capture.                                                                                                                       |
| destroy               | -                                             | -                                                               | void        | All          | Destroy camera preview.                                                                                                                         |
| setToken              | `token: String`                               | A JSON Web Token issued by BGC                                  | void        | All          | Necessary to make requests to our APIs                                                                                                          |
| toggleCameraLens      | -                                             | -                                                               | void        | All          | Set camera lens facing front or back.                                                                                                           |
| setCameraLens         | `cameraLens: String`                          | `"front"` `"back"`                                              | void        | All          | Set camera to use "front" or "back" lens. Default value is "front".                                                                             |
| getCameraLens         | -                                             | -                                                               | Int         | All          | Return `Int` that represents lens face state: 0 for front 1 for back camera.                                                                    |
| setDebugMode          | `enable: Boolean`                             | `true` or `false`                                               | void        | All          | Enable/disable debug mode. It will show on screen image parameters, useful for development.                                                     |
| setDetectionBox       | `enable: Boolean`                             | `true` or `false`.                                              | void        | All          | Enable/disable detection box when face/qrcode detected. The detection box is the the face/qrcode bounding box normalized to UI.                 |
| setDetectionBoxColor  | `alpha: Int, red: Int, green: Int, blue: Int` | Value between `0` and `1`.                                      | void        | All          | Set detection box ARGB color. Default value is `(100, 255, 255, 255)`.                                                                          |
| setNumberOfImages     | `numberOfImages: Int`                         | Any positive `Int` value.                                       | void        | All          | Default value is 0. For value 0 is saved infinity images. When captured images reached the "number os images", the `onEndCapture` is triggered. |
| setCreateFaceOnDetect | `enabled: Boolean`                            | `true` or `false`                                               | void        | FaceView     | Enable/disable create a face on image recognition service when a face detected. Will stop face capture after creation.                          |
| setFaceMatchOnDetect  | `enabled: Boolean, faceId: String`            | `true` and `UUID`                                               | void        | FaceView     | Enable/disable face match on image recognition service when face detected. Will stop after match or unmatch                                     |
| setFaceMatchOnDetect  | `enabled: Boolean`                            | `true` or `false`                                               | void        | FaceView     | Easier way to disable or enable face match on face detect after setting an id.                                                                  |
| setDocumentOCR        | `enable: Boolean, type: String`               | `true` and `"cnh"` or `"rg"`                                    | void        | DocumentView | Enable/disable document creation on OCR service when document detected. Will stop document capture when document is created.                    |
| setSteps              | `steps: Array`                                | `lookUp`, `lookDown`, `lookLeft`, `lookRight`, `smile` or `box` | void        | LivenessView | Sets liveness checks, it will respect array order                                                                                               |
## Events

| Event                	| Parameters                                                                                                                                                                                                     	| Availability          	| Prerequisite                                                               	| Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 	|
|----------------------	|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|-----------------------	|----------------------------------------------------------------------------	|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| onImageCaptured      	| `type: String, count: Int, total: Int, imagePath: String, inferences: ArrayList>`                                                                                                                              	| All                   	| Started camera preview                                                     	| Emitted when the image file is created:  type: '"face"' or '"frame"' count: current index total: total to create imagePath: the image path inferences: each array element is the image inference result.                                                                                                                                                                                                                                                                                                                                                                                                                                    	|
| onEndCapture         	| -                                                                                                                                                                                                              	| All                   	| Started camera prewiew                                                     	| Emitted when the number of image files created is equal of the number of images set (see the method  `setNumberOfImages`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    	|
| onError              	| `error: String`                                                                                                                                                                                                	| All                   	| Started camera preview                                                     	| Emitted when some error unexpected errors happened: error: error message                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    	|
| onMessage            	| `message: String`                                                                                                                                                                                              	| All                   	| Started camera preview                                                     	| Emmited when some not priority event happened: message: message                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             	|
| onPermissionDenied   	| -                                                                                                                                                                                                              	| All                   	| Started camera preview                                                     	| Emitted when called `startPreview` but the camera permission was not granted.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               	|
| onSuccessDialogClose 	| -                                                                                                                                                                                                              	| All                   	| Started camera preview                                                     	| Emitted when the success dialog box are closed                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              	|
| onFaceDetected       	| `x: Int, y: Int, width: Int, height: Int, leftEyeOpenProbability: Float?, rightEyeOpenProbability: Float?, smilingProbability: Float?, headEulerAngleX: Float, headEulerAngleY: Float, headEulerAngleZ: Float` 	| FaceView LivenessView 	| Started camera preview                                                     	| Emmited when a face was detected: x: face x postion (starting at left), y: face y postion (starting at top), width: face width, height: face height, leftEyeOpenProbability?: probability of left eye being open, value between `0` and `1`, rightEyeOpenProbability?: probability of right eye being open, value between `0` and `1` smilingProbability: Float?: probability of smiling, value between `0` and `1` headEulerAngleX: Vertical angle - positive means up, negative is down headEulerAngleY: Horizontal angle - positive means right, negative is left headEulerAngleZ: Tilted angle - positive means left, negative is right 	|
| onFaceUndetected     	| -                                                                                                                                                                                                              	| FaceView LivenessView 	| Started camera preview                                                     	| Emitted when a face was not detected                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        	|
| onFaceCreated        	| `id: String, result: CandidateFaceResult`                                                                                                                                                                      	| FaceView              	| Started camera preview Setted BGC TOKEN Enabled `createFaceOnDetect`       	| Emitted after a `onFaceDetected.  If the face passes our image recognition api validation it will be returned a face identifier string (used later for face match) and a result containing all face parameters detected by our service See more [CandidateFaceResult](#candidate-face-result)                                                                                                                                                                                                                                                                                                                                               	|
| onFaceCreationFail   	| `error: String`                                                                                                                                                                                                	| FaceView              	| Started camera preview Setted BGC TOKEN Enabled  `createFaceOnDetect`      	| Emmited when something does wrotn with request to FaceCreate service                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        	|
| onFaceMatch          	| `id: String, result: CandidateFaceMatchResult`                                                                                                                                                                 	| FaceView              	| Started camera preview Setted BGC TOKEN Enabled  `faceMatchOnDetect`       	| Emitted after a `onFaceDetected.  If the face passes our image recognition api validation it will be returned a face identifier string (used later for face match) and a result containing all face parameters detected by our service See more  [CandidateFaceMatchResult](#candidate-facematch-result)                                                                                                                                                                                                                                                                                                                                    	|
| onFaceUnmatch        	| `id: String, result: CandidateFaceMatchResult`                                                                                                                                                                 	| FaceView              	| Started camera preview Setted BGC TOKEN Enabled  `faceMatchOnDetect`       	| Emitted after a `onFaceDetected.  If the face passes our image recognition api validation it will be returned a face identifier string (used later for face match) and a result containing all face parameters detected by our service See more  [CandidateFaceMatchResult](#candidate-facematch-result)                                                                                                                                                                                                                                                                                                                                    	|
| onFaceMatchFail      	| `error: String`                                                                                                                                                                                                	| FaceView              	| Started camera preview Setted BGC TOKEN Enabled  `faceMatchOnDetect`       	| Emitted when something goes wrong with request to FaceMatch service                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         	|
| onCNHCreated         	| `id: String, result: OCRCNHResult`                                                                                                                                                                             	| DocumentView          	| Started camera preview Setted BGC TOKEN Enabled  `documentOCR` with "cnh"  	| Emitted when a document passes our OCR validation, it will return a document identifier and a result containing document infos See more [OCRCNHResult](#ocr-cnh-result)                                                                                                                                                                                                                                                                                                                                                                                                                                                            	|
| onCNHFailed          	| `error: String`                                                                                                                                                                                                	| DocumentView          	| Started camera preview Setted BGC TOKEN Enabled  `documentOCR`  with "cnh" 	| Emitted when something goes wrong with request to OCR service                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               	|
| onRGCreated          	| `id: String, result: OCRRGResult`                                                                                                                                                                              	| DocumentView          	| Started camera preview Setted BGC TOKEN Enabled  `documentOCR`  with "rg"  	| Emitted when a document passes our OCR validation, it will return a document identifier and a result containing document infos See more [OCRRGResult](#ocr-rg-result)                                                                                                                                                                                                                                                                                                                                                                                                                                                             	|
| onRGFailed           	| `error: String`                                                                                                                                                                                                	| DocumentView          	| Started camera preview Setted BGC TOKEN Enabled  `documentOCR`  with "rg"  	| Emitted when something goes wrong with request to OCR service                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               	|
| onLiveness           	| `liveness: Boolean`                                                                                                                                                                                            	| LivenessView          	| Started camera preview Setted BGC TOKEN Enabled  `setStartLiveness`        	| Emited when all liveness challenges were completed                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          	|

### Candidate Face Result

| Attribute  	| Type       	| Description                                                               	|
|------------	|------------	|---------------------------------------------------------------------------	|
| smile      	| Smile      	| Data class containing `value: Boolean` and `confidance: Double`           	|
| eyeglasses 	| Eyeglasses 	| Data class containing `value: Boolean` and `confidance: Double`           	|
| sunglasses 	| Sunglasses 	| Data class containing `value: Boolean` and `confidance: Double`           	|
| eyesOpen   	| EyesOpen   	| Data class containing `value: Boolean` and `confidance: Double`           	|
| mouthOpen  	| MouthOpen  	| Data class containing `value: Boolean` and `confidance: Double`           	|
| landmarks  	| List       	| List of data class containing `type: String`, `x: Double` and `y: Double` 	|
| gender     	| Gender     	| Data class containing `value: String`                                     	|
| brightness 	| Double     	| Level of brightness                                                       	|
| sharpness  	| Double,    	| Level of sharpness                                                        	|
| confidence 	| Double     	| Level of confidence                                                       	|

### Candidate FaceMatch Result

| Attribute  	| Type    	| Description          	|
|------------	|---------	|----------------------	|
| hasMatch   	| Boolean 	| If it's a face match 	|
| similarity 	| Double  	| Level of similarity  	|
| brightness 	| Double  	| Level of brightness  	|
| sharpness  	| Double, 	| Level of sharpness   	|
| confidence 	| Double  	| Level of confidence  	|

### OCR CNH Result

| Attribute                 	| Type     	| Description                            	|
|---------------------------	|----------	|----------------------------------------	|
| name                     	| String? 	| Driver's name                          	|
| renach                   	| String? 	| CNH's renach                           	|
| fatherName               	| String? 	| Driver's father name                   	|
| identityCardNumber       	| String? 	| Driver's rg                            	|
| motherName               	| String? 	| Driver's mother name                   	|
| birthDate                	| String? 	| Driver's birth date                    	|
| issuingAuthority         	| String? 	| CNH's issuer authority                 	|
| lastModificationDate     	| String? 	| CNH's renovation date                  	|
| identityCardIssuingState 	| String? 	| RG's issuer authority                  	|
| docEmissionPlace         	| String? 	| RG's emission state                    	|
| registrationNumber       	| String? 	| Driver's cnh number                    	|
| cpf                      	| String? 	| Driver's cpf                           	|
| paidActivity             	| String? 	| Driver's ability to do paid activities 	|
| placeOfEmission          	| String? 	| CNH's place of emission                	|
| expirationDate           	| String? 	| CNH's expiration date                  	|
| notes                    	| String? 	| CNH's extra notes                      	|
| firstLicenseDate         	| String? 	| Driver's first licence date            	|
| category                 	| String?  	| CNH's category                         	|

### OCR RG Result

| Attribute 	| Type    	| Description 	|
|-----------	|---------	|-------------	|
| name      	| String? 	| RG's name   	|
### Errors

Pre-define key error constants used by the `onError` event.

| KeyError                    	| Description                                                    	|
|-----------------------------	|----------------------------------------------------------------	|
| INVALID_CAMERA_LENS         	| Tried to input invalid camera lens.                            	|
| INVALID_NUMBER_OF_IMAGES    	| Tried to input invalid face/frame number of images to capture. 	|
| INVALID_DETECTION_BOX_COLOR 	| Tried to input invalid detection box ARGB value color.         	|
| INVALID_FACE_MATCH_PARAMS   	| Tried to set face match true without face id                   	|
| INVALID_OCR_PARAMS          	| Tried to set face match true without valid type                	|

# Services

## BGC Image Recognition

Using image data extraction and recognition techniques, this component is able to recognize and find faces in images.

See more in our
[wiki](https://wiki.bgcbrasil.com.br/en/public/components/bgc-image-recognition)

## BGC OCR

Using the latest techniques in machine learning the API will provide fast and reliable recognition of several documents.

For that we only need to recieve a Base 64 encoded image and it's respective document type, after that the endpoint will return an object containing all the extracted data. 

See more in our [wiki](https://wiki.bgcbrasil.com.br/en/public/components/bgc-ocr   )