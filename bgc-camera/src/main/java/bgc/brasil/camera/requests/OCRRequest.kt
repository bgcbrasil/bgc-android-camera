package bgc.brasil.camera.requests

import android.graphics.Bitmap
import bgc.brasil.camera.interfaces.OCRApi
import bgc.brasil.camera.listeners.CameraCallbacks
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.models.CaptureOptions
import bgc.brasil.camera.models.DocumentFailedReasons
import bgc.brasil.camera.models.FaceFailedReasons
import bgc.brasil.camera.models.OCRCNHResponse
import bgc.brasil.camera.models.OCRCNHResult
import bgc.brasil.camera.models.OCRRGResponse
import bgc.brasil.camera.models.OCRRequestBody
import bgc.brasil.camera.utils.bitMapToString
import bgc.brasil.camera.utils.reduceBitmap
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OCRRequest(private val cameraCallback: CameraCallbacks) {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://ocr.bgcbrasil.com.br")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val api: OCRApi = retrofit.create(OCRApi::class.java)

    // Camera Event object - Events that our lib exposes
    var cameraEvents: CameraEvents = object : CameraEvents() { }

    fun createCNHDocument(bitmap: Bitmap): Boolean {
        val base64Image = bitMapToString(reduceBitmap(bitmap))

        val request = OCRRequestBody("cnh", base64Image, false)

        val call = api.createCNHSearch(request, CaptureOptions.token)

        call.enqueue(object : Callback<OCRCNHResponse> {
            override fun onFailure(call: Call<OCRCNHResponse>, t: Throwable) {
                cameraCallback.onAnalyzerRequestFail(FaceFailedReasons.Request)
            }

            override fun onResponse(call: Call<OCRCNHResponse>, response: Response<OCRCNHResponse>) {
                if (response.isSuccessful) {
                    val ocrCNHResponse = response.body()
                    val result: OCRCNHResult? = ocrCNHResponse?.search?.result

                    if (
                        result?.paidActivity !== null &&
                        result.birthDate !== null &&
                        result.registrationNumber !== null &&
                        result.name !== null &&
                        result.cpf !== null &&
                        result.category !== null &&
                        result.expirationDate !== null
                    ) {
                        cameraEvents.onCNHCreated(
                            ocrCNHResponse.search.id,
                            ocrCNHResponse.search.result
                        )

                        cameraCallback.onAnalyzerRequestSuccess()
                    } else {
                        cameraCallback.onAnalyzerRequestFail(DocumentFailedReasons.PoorQuality)

                        cameraEvents.onCNHCreationFail(
                            result.toString()
                        )
                    }
                } else {
                    val gson = Gson()
                    val ocrResponse = gson.fromJson(response.errorBody()?.charStream(), OCRCNHResponse::class.java)

                    val errorMessage = ocrResponse.error ?: ocrResponse.message

                    cameraEvents.onCNHCreationFail(
                        errorMessage
                    )

                    cameraCallback.onAnalyzerRequestFail(DocumentFailedReasons.Unexpected)
                }
            }
        })

        cameraCallback.onAnalyzerRequestStarted()

        return true
    }

    fun createRGDocument(bitmap: Bitmap): Boolean {
        val base64Image = bitMapToString(bitmap)

        val request = OCRRequestBody("rg", base64Image, true)

        val call = api.createRGSearch(request, CaptureOptions.token)

        call.enqueue(object : Callback<OCRRGResponse> {
            override fun onFailure(call: Call<OCRRGResponse>, t: Throwable) {
                cameraCallback.onAnalyzerRequestFail(FaceFailedReasons.Request)
            }

            override fun onResponse(call: Call<OCRRGResponse>, response: Response<OCRRGResponse>) {
                if (response.isSuccessful) {
                    val ocrCNHResponse = response.body()

                    cameraEvents.onRGCreated(
                        ocrCNHResponse?.search?.id,
                        ocrCNHResponse?.search?.result
                    )

                    cameraCallback.onAnalyzerRequestSuccess()
                } else {
                    val gson = Gson()
                    val ocrResponse = gson.fromJson(response.errorBody()?.charStream(), OCRRGResponse::class.java)

                    val errorMessage = ocrResponse.error ?: ocrResponse.message

                    cameraEvents.onRGCreationFail(
                        errorMessage
                    )

                    cameraCallback.onAnalyzerRequestFail(DocumentFailedReasons.Unexpected)
                }
            }
        })

        cameraCallback.onAnalyzerRequestStarted()

        return true
    }

    companion object {
        private const val TAG = "OCRRequest"
    }
}
