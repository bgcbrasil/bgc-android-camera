package bgc.brasil.camera.requests

import bgc.brasil.camera.interfaces.ImageRecognitionApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ImageRecognition {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://image-recognition.bgcbrasil.com.br")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val api: ImageRecognitionApi = retrofit.create(ImageRecognitionApi::class.java)
}
