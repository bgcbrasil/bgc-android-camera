/**
 * +-+-+-+-+-+-+
 * |y|o|o|n|i|t|
 * +-+-+-+-+-+-+
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Yoonit Camera lib for Android applications                      |
 * | Haroldo Teruya & Victor Goulart @ Cyberlabs AI 2020-2021        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

package bgc.brasil.camera.analyzers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import bgc.brasil.camera.controllers.ComputerVisionController
import bgc.brasil.camera.listeners.CameraCallbacks
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.models.CaptureOptions
import bgc.brasil.camera.utils.mirror
import bgc.brasil.camera.utils.rotate
import bgc.brasil.camera.utils.toRGBBitmap
import bgc.brasil.camera.utils.toYUVBitmap
import java.io.File
import java.io.FileOutputStream

/**
 * Custom camera image analyzer based on frame bounded on [CameraController].
 */
class FrameAnalyzer(
    private val context: Context,
    private val cameraEvents: CameraEvents,
    private val graphicView: bgc.brasil.camera.CameraGraphicView,
    private val cameraCallbacks: CameraCallbacks,
    private val captureOptions: CaptureOptions
) : ImageAnalysis.Analyzer {

    private var analyzerTimeStamp: Long = 0
    private var numberOfImages = 0

    /**
     * Receive image from CameraX API.
     *
     * @param imageProxy image from CameraX API.
     */
    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        this.graphicView.clear()

        if (this.shouldAnalyze(imageProxy)) {
            val mediaImage = imageProxy.image

            mediaImage?.let {

                var frameBitmap: Bitmap = when (captureOptions.colorEncoding) {
                    "YUV" -> mediaImage.toYUVBitmap()
                    else -> mediaImage.toRGBBitmap(context)
                }

                if (captureOptions.cameraLens == CameraSelector.LENS_FACING_BACK) {
                    frameBitmap = frameBitmap
                        .rotate(180f)
                        .mirror()
                }

                // Computer Vision Inference.
                var inferences: ArrayList<android.util.Pair<String, FloatArray>> = arrayListOf()
                if (captureOptions.computerVision.enable) {
                    inferences = ComputerVisionController.getInferences(
                        captureOptions.computerVision.modelMap,
                        frameBitmap
                    )
                }

                // Save image captured.
                var imagePath = ""
                if (captureOptions.saveImageCaptured) {
                    imagePath = this.handleSaveImage(
                        frameBitmap,
                        imageProxy.imageInfo.rotationDegrees.toFloat()
                    )
                }

                // Handle to emit image path and the inference.
                Handler(Looper.getMainLooper()).post {
                    this.handleEmitImageCaptured(imagePath, inferences)
                }
            }
        }

        imageProxy.close()
    }

    /**
     * Check if image is to analyze.
     *
     * @param imageProxy image from CameraX API.
     */
    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    private fun shouldAnalyze(imageProxy: ImageProxy): Boolean {

        if (
            !captureOptions.saveImageCaptured &&
            !captureOptions.computerVision.enable
        ) {
            return false
        }

        if (imageProxy.image == null) {
            return false
        }

        // Process image only within interval equal ANALYZE_TIMER.
        val currentTimestamp = System.currentTimeMillis()
        if (currentTimestamp - this.analyzerTimeStamp < captureOptions.timeBetweenImages) {
            return false
        }
        this.analyzerTimeStamp = currentTimestamp

        return true
    }

    /**
     * Handle emit frame image file created.
     *
     * @param imagePath image file path.
     * @param inferences The computer vision inferences based in the models.
     */
    private fun handleEmitImageCaptured(
        imagePath: String,
        inferences: ArrayList<android.util.Pair<String, FloatArray>>
    ) {

        // process face number of images.
        if (captureOptions.numberOfImages > 0) {
            if (this.numberOfImages < captureOptions.numberOfImages) {
                this.numberOfImages++
                this.cameraEvents.onImageCaptured(
                    "frame",
                    this.numberOfImages,
                    captureOptions.numberOfImages,
                    imagePath,
                    inferences
                )
                return
            }

            this.cameraCallbacks.onStopAnalyzer()
            this.cameraEvents.onEndCapture()
            return
        }

        // process face unlimited.
        this.numberOfImages = (this.numberOfImages + 1) % NUMBER_OF_IMAGES_LIMIT
        this.cameraEvents.onImageCaptured(
            "frame",
            this.numberOfImages,
            captureOptions.numberOfImages,
            imagePath,
            inferences
        )
    }

    /**
     * Handle save file image.
     *
     * @param mediaBitmap the original image bitmap.
     * @param rotationDegrees the rotation degrees to turn the image to portrait.
     * @return the image file path created.
     */
    private fun handleSaveImage(
        mediaBitmap: Bitmap,
        rotationDegrees: Float
    ): String {

        val path = this.context.externalCacheDir.toString()
        val file = File(path, "yoonit-frame-".plus(this.numberOfImages).plus(".jpg"))
        val fileOutputStream = FileOutputStream(file)

        mediaBitmap
            .rotate(rotationDegrees)
            .mirror()
            .compress(
                Bitmap.CompressFormat.JPEG,
                100,
                fileOutputStream
            )

        fileOutputStream.close()

        return file.absolutePath
    }

    companion object {
        private const val TAG = "FrameAnalyzer"
        private const val NUMBER_OF_IMAGES_LIMIT = 25
    }
}
