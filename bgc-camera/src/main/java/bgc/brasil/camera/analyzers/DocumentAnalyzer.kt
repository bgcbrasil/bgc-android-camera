package bgc.brasil.camera.analyzers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.RectF
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import bgc.brasil.camera.controllers.CoordinatesController
import bgc.brasil.camera.controllers.ImageQualityController
import bgc.brasil.camera.listeners.CameraCallbacks
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.utils.rotate
import bgc.brasil.camera.utils.toRGBBitmap
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.objects.ObjectDetection
import com.google.mlkit.vision.objects.ObjectDetector
import com.google.mlkit.vision.objects.defaults.ObjectDetectorOptions

class DocumentAnalyzer(
    private val context: Context,
    private val cameraEvents: CameraEvents,
    private val graphicView: bgc.brasil.camera.CameraGraphicView,
    private val cameraCallbacks: CameraCallbacks,
    private val captureOptions: bgc.brasil.camera.models.CaptureOptions
) : ImageAnalysis.Analyzer {
    private val scanner: ObjectDetector = ObjectDetection.getClient(
        ObjectDetectorOptions.Builder()
            .setDetectorMode(ObjectDetectorOptions.SINGLE_IMAGE_MODE)
            .build()
    )
    private val coordinatesController = CoordinatesController(this.graphicView, captureOptions)

    /**
     * Receive image from CameraX API.
     *
     * @param imageProxy image from CameraX API.
     */
    @SuppressLint("UnsafeExperimentalUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        this.detect(
            imageProxy,
            { errorMessage ->
                this.cameraEvents.onError(errorMessage)
            },
            {
                imageProxy.close()
            }
        )
    }

    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    private fun detect(
        imageProxy: ImageProxy,
        onError: (String) -> Unit,
        onComplete: () -> Unit
    ) {
        val mediaImage = imageProxy.image ?: return

        val imageBitmap = mediaImage
            .toRGBBitmap(context)
            .rotate(imageProxy.imageInfo.rotationDegrees.toFloat())

        val inputImage: InputImage = InputImage.fromBitmap(
            imageBitmap,
            0
        )

        this.scanner
            .process(inputImage)
            .addOnSuccessListener { detectedObjects ->
                var detectionBox: RectF? = null
                if (detectedObjects.isNotEmpty()) {
                    val boundingBox = detectedObjects[0].boundingBox

                    detectionBox = this.coordinatesController.getDetectionBox(
                        boundingBox,
                        imageProxy.width.toFloat(),
                        imageProxy.height.toFloat()
                    )

                    // Draw or clean the document detection box.
                    this.graphicView.handleDraw(
                        detectionBox
                    )
                }

                val validDocumentParameters = ImageQualityController.validateDocumentParameters(
                    imageBitmap,
                    detectionBox,
                    Pair(this.graphicView.width, this.graphicView.height)
                )

                cameraCallbacks.onDocumentValidated(validDocumentParameters)
                cameraCallbacks.onDebug("detectionBox: ${detectionBox}\n - ${validDocumentParameters.toString().replace(',', '\n')}")
            }
            .addOnFailureListener { e ->
                this.graphicView.clear()
                onError(e.toString())
                onComplete()
            }
            .addOnCompleteListener {
                onComplete()
            }

        return
    }

    companion object {
        private const val TAG = "DocumentAnalyzer"
    }
}
