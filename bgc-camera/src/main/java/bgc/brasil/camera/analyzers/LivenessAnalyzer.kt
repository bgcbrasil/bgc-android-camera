/**
 * +-+-+-+-+-+-+
 * |y|o|o|n|i|t|
 * +-+-+-+-+-+-+
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Yoonit Camera lib for Android applications                      |
 * | Haroldo Teruya & Victor Goulart @ Cyberlabs AI 2020-2021        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

package bgc.brasil.camera.analyzers

import ai.cyberlabs.yoonit.facefy.Facefy
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.RectF
import android.media.Image
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import bgc.brasil.camera.CameraGraphicView
import bgc.brasil.camera.controllers.ComputerVisionController
import bgc.brasil.camera.controllers.CoordinatesController
import bgc.brasil.camera.enums.LivenessSteps
import bgc.brasil.camera.listeners.CameraCallbacks
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.models.CaptureOptions
import bgc.brasil.camera.utils.crop
import bgc.brasil.camera.utils.mirror
import bgc.brasil.camera.utils.pxToDPI
import bgc.brasil.camera.utils.rotate
import bgc.brasil.camera.utils.scale
import bgc.brasil.camera.utils.toRGBBitmap
import bgc.brasil.camera.utils.toYUVBitmap
import java.io.File
import java.io.FileOutputStream

/**
 * Custom camera image analyzer based on face detection bounded on [CameraController].
 */
class LivenessAnalyzer(
    private val context: Context,
    private val cameraEvents: CameraEvents,
    private val graphicView: CameraGraphicView,
    private val cameraCallbacks: CameraCallbacks,
    private val captureOptions: CaptureOptions
) : ImageAnalysis.Analyzer {
    private var facefy: Facefy = Facefy()
    private var isValid: Boolean = true
    private var numberOfImages = 0
    private val coordinatesController = CoordinatesController(this.graphicView, captureOptions)

    /**
     * Receive image from CameraX API.
     *
     * @param imageProxy image from CameraX API.
     */
    @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
    override fun analyze(imageProxy: ImageProxy) {

        val mediaImage = imageProxy.image ?: return

        val bitmap = mediaImage
            .toRGBBitmap(context)
            .rotate(imageProxy.imageInfo.rotationDegrees.toFloat())
            .mirror()

        var isFaceDetected = false

        this.facefy.detect(
            bitmap,
            { faceDetected ->
                // Get from faceDetected the graphic face bounding box.
                val detectionBox = if (faceDetected != null) {
                    this.coordinatesController.getDetectionBox(
                        faceDetected.boundingBox,
                        imageProxy.width.toFloat(),
                        imageProxy.height.toFloat()
                    )
                } else RectF()

                // Verify if has error on detection box.
                if (this.hasError(detectionBox)) return@detect

                faceDetected?.let {
                    isFaceDetected = true
                    // Transform the camera face contour points to UI graphic coordinates.
                    // Used to draw the face contours.
                    val faceContours = this.coordinatesController.getFaceContours(
                        it.contours,
                        imageProxy.width.toFloat(),
                        imageProxy.height.toFloat()
                    )

                    // Get face bitmap.
                    val faceBitmap: Bitmap = this.getFaceBitmap(
                        mediaImage,
                        it.boundingBox,
                        imageProxy.imageInfo.rotationDegrees.toFloat()
                    )

                    // Draw or clean the face detection box, face blur and face contours.
                    this.graphicView.handleDraw(
                        detectionBox,
                        faceBitmap,
                        faceContours
                    )

                    // Emit face analysis.
                    this.cameraEvents.onFaceDetected(
                        detectionBox.left.pxToDPI(this.context),
                        detectionBox.top.pxToDPI(this.context),
                        detectionBox.width().pxToDPI(this.context),
                        detectionBox.height().pxToDPI(this.context),
                        it.leftEyeOpenProbability,
                        it.rightEyeOpenProbability,
                        it.smilingProbability,
                        it.headEulerAngleX,
                        it.headEulerAngleY,
                        it.headEulerAngleZ
                    )

                    // Check if liveness challenger finished
                    if (captureOptions.livenessStep.currentStep == LivenessSteps.FINISHED) {
                        this.cameraEvents.onLiveness(true)
                    } else {
                        livenessChallenger(
                            it.headEulerAngleX,
                            it.headEulerAngleY,
                            detectionBox,
                            it.smilingProbability
                        )
                    }

                    // Computer Vision Inference.
                    val inferences: ArrayList<android.util.Pair<String, FloatArray>> =
                        if (captureOptions.computerVision.enable)
                            ComputerVisionController.getInferences(
                                captureOptions.computerVision.modelMap,
                                faceBitmap
                            )
                        else arrayListOf()

                    // Save image captured.
                    val imagePath =
                        if (captureOptions.saveImageCaptured) this.handleSaveImage(faceBitmap)
                        else ""

                    // Handle to emit image path and the inferences.
                    this.handleEmitImageCaptured(imagePath, inferences)
                }
            },
            { errorMessage ->
                this.cameraEvents.onError(errorMessage)
            },
            {
                imageProxy.close()
                this.cameraCallbacks.onFaceFrame(isFaceDetected)
            }
        )
    }

    /**
     * Liveness challenger
     */
    private fun livenessChallenger(
        headEulerAngleX: Float,
        headEulerAngleY: Float,
        faceBox: RectF,
        smilingProbability: Float?
    ) {
        val step = captureOptions.livenessStep.currentStep
        val nextStep = captureOptions.livenessStep.nextStep!! // nextStep its verified outside function
        val box = captureOptions.livenessBoxParameters
        val widthOffset = box.width() * 0.08
        val heightOffset = box.height() * 0.08
        val minFaceArea = (box.width() * box.height()) * 0.5

        this.cameraCallbacks.onDebug(
            """
            AngleX: $headEulerAngleX
            AngleY: $headEulerAngleY
            Smiling: $smilingProbability
                    
            FaceBox:
            Left: ${faceBox.left}
            Right: ${faceBox.right}
            Bottom: ${faceBox.bottom}
            Top: ${faceBox.top}
            Area: ${faceBox.width() * faceBox.height()}
            MinAreaAllowed: $minFaceArea
                       
            Box:
            Left: ${box.left}
            Right: ${box.right}
            Bottom: ${box.bottom}
            Top: ${box.top}
            Area: ${box.width() * box.height()}
            
            
            Screen height: ${Resources.getSystem().displayMetrics.heightPixels}
            Screen width : ${Resources.getSystem().displayMetrics.widthPixels}
        """
        )

        var isComplete = when (step) {
            LivenessSteps.FINISHED,
            LivenessSteps.NONE -> true
            LivenessSteps.BOX ->
                box.left - widthOffset <= faceBox.left &&
                    box.right + widthOffset >= faceBox.right &&
                    box.top - heightOffset <= faceBox.top &&
                    box.bottom + heightOffset >= faceBox.bottom && (faceBox.width() * faceBox.height()) >= minFaceArea

            LivenessSteps.LOOKUP -> headEulerAngleX > 25f
            LivenessSteps.LOOKDOWN -> headEulerAngleX < -25f
            LivenessSteps.LOOKLEFT -> headEulerAngleY > 25f
            LivenessSteps.LOOKRIGTH -> headEulerAngleY > 25f
            LivenessSteps.SMILE -> smilingProbability == null || smilingProbability > 0.80f
        }

        if (isComplete) {
            captureOptions.livenessStep = nextStep
            cameraCallbacks.onLivenessChallengeChange(nextStep.currentStep)
            // need to render a box
            cameraCallbacks.onBox(nextStep.currentStep === LivenessSteps.BOX)
        }
    }

    private fun hasError(detectionBox: RectF): Boolean {
        // Get error if exist in the detectionBox.
        val error = this.coordinatesController.getError(
            detectionBox
        )

        // Emit once if exist error in the closestFace or detectionBox.
        return this.handleError(error)
    }

    private fun handleError(error: String?): Boolean {
        error?.let {
            if (this.isValid) {
                this.isValid = false
                this.graphicView.clear()
                if (error != "") {
                    this.cameraEvents.onMessage(error)
                }
                this.cameraEvents.onFaceUndetected()
            }
            return true
        }

        this.isValid = true

        return false
    }

    /**
     * Get face bitmap:
     *
     * 1. Color encoding if necessary;
     * 2. Rotate image if necessary;
     * 3. Mirror image if necessary;
     * 4. Crop image if necessary;
     * 5. Scale image if necessary;
     *
     * @param mediaImage The camera frame image;
     * @param boundingBox The face detected bounding box;
     * @param cameraRotation The camera rotation;
     *
     * @return the face bitmap.
     */
    private fun getFaceBitmap(
        mediaImage: Image,
        boundingBox: Rect,
        cameraRotation: Float
    ): Bitmap {

        val colorEncodedBitmap: Bitmap = when (captureOptions.colorEncoding) {
            "YUV" -> mediaImage.toYUVBitmap()
            else -> mediaImage.toRGBBitmap(context)
        }

        var faceBitmap: Bitmap = colorEncodedBitmap
            .rotate(cameraRotation)
            .mirror()

        faceBitmap = faceBitmap.crop(
            boundingBox.scale(
                captureOptions.detectionTopSize,
                captureOptions.detectionRightSize,
                captureOptions.detectionBottomSize,
                captureOptions.detectionLeftSize
            )
        )

        if (captureOptions.cameraLens == CameraSelector.LENS_FACING_BACK) {
            faceBitmap = faceBitmap.mirror()
        }

        return Bitmap.createScaledBitmap(
            faceBitmap,
            captureOptions.imageOutputWidth,
            captureOptions.imageOutputHeight,
            false
        )
    }

    /**
     * Handle emit face image file created.
     *
     * @param imagePath The image file path.
     * @param inferences The computer vision inferences based in the models.
     */
    private fun handleEmitImageCaptured(
        imagePath: String,
        inferences: ArrayList<android.util.Pair<String, FloatArray>>
    ) {
        if (imagePath == "") return

        // process face number of images.
        if (captureOptions.numberOfImages > 0) {
            if (this.numberOfImages < captureOptions.numberOfImages) {
                this.numberOfImages++
                this.cameraEvents.onImageCaptured(
                    "face",
                    this.numberOfImages,
                    captureOptions.numberOfImages,
                    imagePath,
                    inferences
                )
                return
            }

            this.cameraCallbacks.onStopAnalyzer()
            this.cameraEvents.onEndCapture()
            return
        }

        // process face unlimited.
        this.numberOfImages = (this.numberOfImages + 1) % NUMBER_OF_IMAGES_LIMIT
        this.cameraEvents.onImageCaptured(
            "face",
            this.numberOfImages,
            captureOptions.numberOfImages,
            imagePath,
            inferences
        )
    }

    /**
     * Handle save file image.
     *
     * @param faceBitmap the face bitmap.
     *
     * @return the image file path created.
     */
    private fun handleSaveImage(faceBitmap: Bitmap): String {
        val path = this.context.externalCacheDir.toString()
        val file = File(path, "bgc-face-".plus(this.numberOfImages).plus(".jpg"))
        val fileOutputStream = FileOutputStream(file)

        faceBitmap.compress(
            Bitmap.CompressFormat.JPEG,
            100,
            fileOutputStream
        )

        fileOutputStream.close()

        return file.absolutePath
    }

    companion object {
        private const val TAG = "FaceAnalyzer"
        private const val NUMBER_OF_IMAGES_LIMIT = 25
    }
}
