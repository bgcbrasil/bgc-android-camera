package bgc.brasil.camera

import ai.cyberlabs.yoonit.camera.R
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.camera.view.PreviewView
import androidx.constraintlayout.widget.ConstraintLayout
import bgc.brasil.camera.controllers.FaceController
import bgc.brasil.camera.enums.CaptureType
import bgc.brasil.camera.enums.FaceParametersPositions
import bgc.brasil.camera.enums.ImageRecognitionApi
import bgc.brasil.camera.listeners.CameraViewsEvents
import bgc.brasil.camera.models.FaceFailedReasons
import bgc.brasil.camera.models.FaceParameters
import bgc.brasil.camera.models.KeyError
import com.google.android.flexbox.FlexboxLayout
import java.util.Timer
import kotlin.concurrent.schedule

class FaceView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : CameraView(
    context,
    attrs,
    defStyle,
    defStyleRes
) {

    /**
     * Inflate CameraView layout and instantiate.
     */
    init {

        LayoutInflater.from(context).inflate(
            R.layout.faceview_layout,
            this,
            true
        )
        this.cameraController = FaceController(
            context,
            findViewById<PreviewView>(R.id.previewView),
            findViewById<CameraGraphicView>(R.id.graphicView),
            captureOptions
        )

        this.cameraController.cameraViewsEvents = object : CameraViewsEvents() {
            override fun onLoadingChange(loading: Boolean) {
                if (loading) startLoading(findViewById<ConstraintLayout>(R.id.appBody))
                else stopLoading(findViewById<ConstraintLayout>(R.id.appBody))
            }

            override fun onSuccessMessage(onClose: () -> Unit) {
                when (captureOptions.imageRecognitionApi) {
                    ImageRecognitionApi.CREATE -> messageView.onSuccessFaceCreate { onClose() }
                    ImageRecognitionApi.MATCH -> messageView.onSuccessFaceMatch { onClose() }
                    else -> { /* nothing */ }
                }
            }

            override fun onFaceValidated(params: FaceParameters) {
                setEyesOpenAlert(params.eyesOpen)
                setSmilingAlert(params.smiling)
                setLightAlert(params.bright, params.dark)
                setDistanceAlert(params.distance)
                setCenteredAlert(params.centered)
                setSharpAlert(params.sharp)
            }

            override fun onDebug(message: String) {
                findViewById<TextView>(R.id.debugFaceParameters).text = message
            }

            override fun onFrame(isFaceDetected: Boolean) {
                setFacePresenceAlert(isFaceDetected)
            }

            override fun onFailFaceMessage(errorCode: String, tryAgain: () -> Unit) {
                when (errorCode) {
                    FaceFailedReasons.NotMatch -> messageView.onFaceNotMatched { tryAgain() }
                    FaceFailedReasons.TooManyFaces -> messageView.onTooManyFaces { tryAgain() }
                    FaceFailedReasons.OpenMouth -> messageView.onMouthOpened { tryAgain() }
                    FaceFailedReasons.WearingGlasses -> messageView.onWearingGlasses { tryAgain() }
                    FaceFailedReasons.PoorQuality -> messageView.onPoorQuality { tryAgain() }
                    FaceFailedReasons.NotFoundFaceId -> messageView.notFoundFaceId { tryAgain() }
                    FaceFailedReasons.ClosedEyes -> messageView.onClosedEyes { tryAgain() }
                    FaceFailedReasons.NoFace -> messageView.onNoFaceRequest { tryAgain() }
                    FaceFailedReasons.Smiling -> messageView.onSmiling { tryAgain() }
                    else -> messageView.onUnexpectedError { tryAgain() } // Unexpected or request error
                }
            }
        }
    }

    override fun startPreview() {
        captureOptions.type = CaptureType.FACE

        Timer("SettingUp", false).schedule(7000) {
            isFacePresenceAlertEnabled = true
        }

        super.startPreview()
    }

    override fun setDebugMode(enable: Boolean) {
        findViewById<TextView>(R.id.debugFaceParameters).visibility = if (enable) View.VISIBLE else View.INVISIBLE
    }

    /**
     * Set to enable/disable the feature to create face on BGC's Image Recognition service on face detection.
     *
     * @param enable The indicator to enable or disable the face creation.
     * Default value is false.
     */
    fun setCreateFaceOnDetect(enable: Boolean) {
        captureOptions.imageRecognitionApi = if (enable) ImageRecognitionApi.CREATE else ImageRecognitionApi.NONE
        if (enable) findViewById<TextView>(R.id.instructions_header).text = resources.getString(R.string.face_header_create)
    }

    /**
     * Set to first setup the feature to match face on BGC's Image Recognition service on face detection.
     * @param enable The indicator to enable or disable the face match.
     * @param faceId = faceId to be matched
     * Default value is false and null
     */
    fun setFaceMatchOnDetect(enable: Boolean, faceId: String) {
        captureOptions.faceId = faceId
        captureOptions.imageRecognitionApi = if (enable) ImageRecognitionApi.MATCH else ImageRecognitionApi.NONE
        if (enable) findViewById<TextView>(R.id.instructions_header).text = resources.getString(R.string.face_header_match)
    }

    /**
     * Set to enable/disable the feature to match face on BGC's Image Recognition service on face detection.
     *
     * @param enable The indicator to enable or disable the face match.
     * Default value is false.
     */
    fun setFaceMatchOnDetect(enable: Boolean) {
        if (enable && captureOptions.faceId == null) {
            throw java.lang.IllegalArgumentException(KeyError.INVALID_FACE_MATCH_PARAMS)
        }
        captureOptions.imageRecognitionApi = if (enable) ImageRecognitionApi.MATCH else ImageRecognitionApi.NONE
        if (enable) findViewById<TextView>(R.id.instructions_header).text = resources.getString(R.string.face_header_match)
    }

    private var isFacePresenceAlertEnabled = false

    private fun setFacePresenceAlert(isDetected: Boolean) {
        if (!isFacePresenceAlertEnabled) return

        if (isDetected) {
            findViewById<FlexboxLayout>(R.id.instructions1).visibility = View.VISIBLE
            findViewById<FlexboxLayout>(R.id.instructions2).visibility = View.VISIBLE
            findViewById<FlexboxLayout>(R.id.instructions3).visibility = View.VISIBLE
            findViewById<FlexboxLayout>(R.id.instructions4).visibility = View.VISIBLE
            findViewById<FlexboxLayout>(R.id.instructions5).visibility = View.VISIBLE
            findViewById<FlexboxLayout>(R.id.instructions6).visibility = View.VISIBLE
            findViewById<FlexboxLayout>(R.id.instructions7).visibility = View.GONE
        } else {
            findViewById<FlexboxLayout>(R.id.instructions1).visibility = View.GONE
            findViewById<FlexboxLayout>(R.id.instructions2).visibility = View.GONE
            findViewById<FlexboxLayout>(R.id.instructions3).visibility = View.GONE
            findViewById<FlexboxLayout>(R.id.instructions4).visibility = View.GONE
            findViewById<FlexboxLayout>(R.id.instructions5).visibility = View.GONE
            findViewById<FlexboxLayout>(R.id.instructions6).visibility = View.GONE
            findViewById<FlexboxLayout>(R.id.instructions7).visibility = View.VISIBLE
        }
    }

    private fun setEyesOpenAlert(eyesOpen: Boolean) {
        if (eyesOpen) {
            findViewById<TextView>(R.id.instructions2_text).text = resources.getString(R.string.instructions_face_success_eyes_open)
            findViewById<ImageView>(R.id.instructions2_icon).setImageResource(R.drawable.ic_check_icon)
        } else {
            findViewById<TextView>(R.id.instructions2_text).text = resources.getString(R.string.instructions_face_fail_eyes_open)
            findViewById<ImageView>(R.id.instructions2_icon).setImageResource(R.drawable.ic_error_icon)
        }
    }

    private fun setSmilingAlert(smiling: Boolean) {
        if (!smiling) {
            findViewById<TextView>(R.id.instructions3_text).text = resources.getString(R.string.instructions_face_success_not_smiling)
            findViewById<ImageView>(R.id.instructions3_icon).setImageResource(R.drawable.ic_check_icon)
        } else {
            findViewById<TextView>(R.id.instructions3_text).text = resources.getString(R.string.instructions_face_fail_not_smiling)
            findViewById<ImageView>(R.id.instructions3_icon).setImageResource(R.drawable.ic_error_icon)
        }
    }

    private fun setLightAlert(dark: Boolean, bright: Boolean) {
        if (!(dark || bright)) {
            findViewById<TextView>(R.id.instructions1_text).text = resources.getString(R.string.instructions_face_success_light)
            findViewById<ImageView>(R.id.instructions1_icon).setImageResource(R.drawable.ic_check_icon)
        } else if (dark) {
            // dark message
            findViewById<TextView>(R.id.instructions1_text).text = resources.getString(R.string.instructions_face_fail_dark)
            findViewById<ImageView>(R.id.instructions1_icon).setImageResource(R.drawable.ic_error_icon)
        } else {
            // too bright message
            findViewById<TextView>(R.id.instructions1_text).text = resources.getString(R.string.instructions_face_fail_light)
            findViewById<ImageView>(R.id.instructions1_icon).setImageResource(R.drawable.ic_error_icon)
        }
    }

    private fun setDistanceAlert(distance: FaceParametersPositions) {
        when (distance) {
            FaceParametersPositions.OK -> {
                findViewById<TextView>(R.id.instructions4_text).text = resources.getString(R.string.instructions_face_success_distance)
                findViewById<ImageView>(R.id.instructions4_icon).setImageResource(R.drawable.ic_check_icon)
            }
            FaceParametersPositions.NEAR -> {
                findViewById<TextView>(R.id.instructions4_text).text = resources.getString(R.string.instructions_face_fail_distance_near)
                findViewById<ImageView>(R.id.instructions4_icon).setImageResource(R.drawable.ic_error_icon)
            }
            else -> {
                findViewById<TextView>(R.id.instructions4_text).text = resources.getString(R.string.instructions_face_fail_distance_far)
                findViewById<ImageView>(R.id.instructions4_icon).setImageResource(R.drawable.ic_error_icon)
            }
        }
    }

    private fun setCenteredAlert(centered: Boolean) {
        if (centered) {
            findViewById<TextView>(R.id.instructions5_text).text = resources.getString(R.string.instructions_face_success_centered)
            findViewById<ImageView>(R.id.instructions5_icon).setImageResource(R.drawable.ic_check_icon)
        } else {
            findViewById<TextView>(R.id.instructions5_text).text = resources.getString(R.string.instructions_face_fail_centered)
            findViewById<ImageView>(R.id.instructions5_icon).setImageResource(R.drawable.ic_error_icon)
        }
    }

    private fun setSharpAlert(sharp: Boolean) {
        if (sharp) {
            findViewById<TextView>(R.id.instructions6_text).text = resources.getString(R.string.instructions_face_success_sharp)
            findViewById<ImageView>(R.id.instructions6_icon).setImageResource(R.drawable.ic_check_icon)
        } else {
            findViewById<TextView>(R.id.instructions6_text).text = resources.getString(R.string.instructions_face_fail_sharp)
            findViewById<ImageView>(R.id.instructions6_icon).setImageResource(R.drawable.ic_error_icon)
        }
    }

//    companion object {
//        private const val TAG = "FaceView"
//    }
}
