package bgc.brasil.camera.graphics

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import bgc.brasil.camera.models.CaptureOptions

/**
 * This View responsible to draw check face position box
 */

class LivenessRectangleView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(
    context,
    attrs,
    defStyleAttr
) {

    override fun draw(canvas: Canvas) {
        this.drawLivenessFaceBox(canvas)

        super.draw(canvas)
    }

    fun drawLivenessFaceBox(canvas: Canvas) {
        val screenWidth: Int = Resources.getSystem().displayMetrics.widthPixels
        val screenHeight: Int = Resources.getSystem().displayMetrics.heightPixels

        val heightSize = screenHeight * 0.40
        val widthSize = screenWidth * 0.70

        val topRandom = (0..(height - heightSize.toInt())).random().toFloat()
        val lefRandom = (0..(width - widthSize.toInt())).random().toFloat()

        val livenessBoxpaint = Paint()
        livenessBoxpaint.style = Paint.Style.STROKE
        livenessBoxpaint.strokeWidth = 8f
        livenessBoxpaint.pathEffect = DashPathEffect(floatArrayOf(100f, 20f), 0f)

        livenessBoxpaint.color = Color.YELLOW

        val livenessBoxRect = RectF(
            lefRandom,
            topRandom,
            lefRandom + widthSize.toFloat(),
            topRandom + heightSize.toFloat()
        )

        canvas.drawRoundRect(livenessBoxRect, 10f, 10f, livenessBoxpaint)

        CaptureOptions.livenessBoxPosition = livenessBoxRect.right.toInt()

        CaptureOptions.livenessBoxParameters = RectF(
            livenessBoxRect.left,
            livenessBoxRect.top,
            livenessBoxRect.right,
            livenessBoxRect.bottom
        )
    }
}
