package bgc.brasil.camera.models

data class OCRRGResult(
    val name: String?
)

data class OCRRGSearch(
    val id: String,
    val type: String,
    val s3EncryptedLink: String,
    val result: OCRRGResult
)
