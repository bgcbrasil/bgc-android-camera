package bgc.brasil.camera.models

object FaceFailedReasons {
    const val Unexpected: String = "UNEXPECTED"
    const val TooManyFaces: String = "TOOMANYFACES"
    const val PoorQuality: String = "POORQUALITY"
    const val NotFoundFaceId: String = "NOTFOUNDFACEID"
    const val WearingGlasses: String = "WEARINGGLASSES"
    const val Request: String = "REQUEST"
    const val ClosedEyes: String = "CLOSEDEYES"
    const val OpenMouth: String = "OPENMOUTH"
    const val Smiling: String = "SMILING"
    const val NoFace: String = "NOFACE"
    const val NotMatch: String = "NOTMATCH"
}
