package bgc.brasil.camera.models

data class CandidateFaceMatchResponse(
    val error: String?,
    val message: String,
    val candidateFaceMatch: CandidateFaceMatch?,
    val errorCode: String?
)
