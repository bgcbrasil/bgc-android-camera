package bgc.brasil.camera.models

data class CandidateFaceMatch(
    val id: String,
    val faceId: String,
    val principalGroup: String,
    val principalId: String,
    val createdAt: String,
    val result: CandidateFaceMatchResult
)

data class CandidateFaceMatchResult(
    val hasMatch: Boolean,
    val similarity: Double,
    val brightness: Double,
    val sharpness: Double,
    val confidence: Double
)
