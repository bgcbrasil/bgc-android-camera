package bgc.brasil.camera.models

import bgc.brasil.camera.enums.FaceParametersPositions

// FaceParameters is initialized with a perfect face
data class FaceParameters(
    var distance: FaceParametersPositions = FaceParametersPositions.OK,
    var xPosition: FaceParametersPositions = FaceParametersPositions.OK,
    var yPosition: FaceParametersPositions = FaceParametersPositions.OK,
    var xAngle: FaceParametersPositions = FaceParametersPositions.OK,
    var yAngle: FaceParametersPositions = FaceParametersPositions.OK,
    var zAngle: FaceParametersPositions = FaceParametersPositions.OK,
    var eyesOpen: Boolean = true,
    var smiling: Boolean = false,
    var dark: Boolean = false,
    var bright: Boolean = false,
    var sharp: Boolean = true,
    var centered: Boolean = true,
    var valid: Boolean = true
)
