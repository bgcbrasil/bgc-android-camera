package bgc.brasil.camera.models

import bgc.brasil.camera.enums.DocumentParametersPositions

// DocumentParameters is initialized with a perfect document
data class DocumentParameters(
    var dark: Boolean = false,
    var bright: Boolean = false,
    var sharp: Boolean = true,
    var distance: DocumentParametersPositions = DocumentParametersPositions.OK,
    var valid: Boolean = true
)
