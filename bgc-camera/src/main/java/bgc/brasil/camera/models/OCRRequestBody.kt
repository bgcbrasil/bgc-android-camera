package bgc.brasil.camera.models

data class OCRRequestBody(
    val type: String,
    val base64: String,
    val enhancedAccuracy: Boolean?
)
