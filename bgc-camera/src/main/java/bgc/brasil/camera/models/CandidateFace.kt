package bgc.brasil.camera.models

data class CandidateFace(
    val id: String,
    val result: CandidateFaceResult,
    val principalGroup: String,
    val principalId: String,
    val createdAt: String,
    val updatedAt: String
)

data class CandidateFaceResult(
    val smile: Smile,
    val eyeglasses: Eyeglasses,
    val sunglasses: Sunglasses,
    val eyesOpen: EyesOpen,
    val mouthOpen: MouthOpen,
    val landmarks: List<Landmark>,
    val gender: Gender,
    val brightness: Double,
    val sharpness: Double,
    val confidence: Double
)

data class Smile(
    val value: Boolean,
    val confidence: Double
)

data class Eyeglasses(
    val value: Boolean,
    val confidence: Double
)

data class Sunglasses(
    val value: Boolean,
    val confidence: Double
)

data class EyesOpen(
    val value: Boolean,
    val confidence: Double
)

data class MouthOpen(
    val value: Boolean,
    val confidence: Double
)

data class Landmark(
    val type: String,
    val x: Double,
    val y: Double
)

data class Gender(
    val value: String
)
