package bgc.brasil.camera.models

data class OCRRGResponse(
    val error: String?,
    val message: String,
    val search: OCRRGSearch?
)
