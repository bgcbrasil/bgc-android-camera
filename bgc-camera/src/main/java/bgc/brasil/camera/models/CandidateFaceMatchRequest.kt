package bgc.brasil.camera.models

data class CandidateFaceMatchRequest(
    val faceBiometrics: String,
    val faceId: String
)
