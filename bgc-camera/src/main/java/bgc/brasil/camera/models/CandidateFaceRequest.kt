package bgc.brasil.camera.models

data class CandidateFaceRequest(
    val faceBiometrics: String
)
