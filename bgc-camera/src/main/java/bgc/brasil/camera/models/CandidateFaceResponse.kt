package bgc.brasil.camera.models

data class CandidateFaceResponse(
    val error: String?,
    val message: String,
    val candidateFace: CandidateFace?,
    val errorCode: String?
)
