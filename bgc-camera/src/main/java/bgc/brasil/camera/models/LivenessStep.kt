package bgc.brasil.camera.models

import bgc.brasil.camera.enums.LivenessSteps

class LivenessStep(val currentStep: LivenessSteps, val nextStep: LivenessStep? = null)
