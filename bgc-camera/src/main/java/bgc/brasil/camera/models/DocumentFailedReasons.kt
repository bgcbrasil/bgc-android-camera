package bgc.brasil.camera.models

object DocumentFailedReasons {
    const val Unexpected: String = "UNEXPECTED"
    const val PoorQuality: String = "POORQUALITY"
}
