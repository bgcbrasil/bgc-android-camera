/**
 * +-+-+-+-+-+-+
 * |y|o|o|n|i|t|
 * +-+-+-+-+-+-+
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Yoonit Camera lib for Android applications                      |
 * | Haroldo Teruya & Victor Goulart @ Cyberlabs AI 2020-2021        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

package bgc.brasil.camera.models

import android.graphics.Color
import android.graphics.RectF
import androidx.camera.core.CameraSelector
import bgc.brasil.camera.enums.CaptureType
import bgc.brasil.camera.enums.ImageRecognitionApi
import bgc.brasil.camera.enums.LivenessSteps
import bgc.brasil.camera.enums.OCRApi

/**
 * Model to set [CameraView] capture features options.
 */
object CaptureOptions {

    // Liveness steps
    var livenessStep: LivenessStep = LivenessStep(LivenessSteps.NONE)

    // livenessBox parameters
    var livenessBoxParameters = RectF()

    // LivenessBoxPosition
    var livenessBoxPosition: Int = 0

    // Region of interesting.
    var roi: ROI = ROI()

    // Computer vision models.
    var computerVision: ComputerVision = ComputerVision()

    // Camera image capture type: NONE, FACE, QRCODE and FRAME.
    var type: CaptureType = CaptureType.NONE

    // Camera lens facing: CameraSelector.LENS_FACING_FRONT and CameraSelector.LENS_FACING_BACK.
    var cameraLens: Int = CameraSelector.LENS_FACING_FRONT

    // Face/Frame capture number of images. 0 capture unlimited.
    var numberOfImages: Int = 0

    // Face/Frame/Document capture time between images in milliseconds.
    var timeBetweenImages: Long = 1000

    // Face/Frame capture image width to create.
    var imageOutputWidth: Int = 200

    // Face/Frame capture image height to create.
    var imageOutputHeight: Int = 200

    // Face/Frame save images captured.
    var saveImageCaptured: Boolean = false

    // Token that will be used to call BGC's services.
    var token: String = ""

    // Specifies which API will be called
    var imageRecognitionApi: ImageRecognitionApi = ImageRecognitionApi.NONE
    var ocrApi: OCRApi = OCRApi.NONE

    // FaceId to be used on BGC's Image Recognition service on face match.`
    var faceId: String? = null

    // Draw or not the face/qrcode detection box.
    var detectionBox: Boolean = false

    /**
     * Face/qrcode minimum size to detect in percentage related with the camera preview.
     * This variable is the detection box percentage in relation with the UI graphic view.
     * The value must be between `0` and `1`.
     */
    var minimumSize: Float = 0.0f

    /**
     * Face/qrcode maximum size to detect in percentage related with the camera preview.
     * This variable is the detection box percentage in relation with the UI graphic view.
     * The value must be between `0` and `1`.
     */
    var maximumSize: Float = 1.0f

    var detectionTopSize: Float = 0.0f
    var detectionRightSize: Float = 0.0f
    var detectionBottomSize: Float = 0.0f
    var detectionLeftSize: Float = 0.0f

    // Detection box color.
    var detectionBoxColor: Int = Color.WHITE

    // Face contours.
    var faceContours: Boolean = false

    // Face contours color.
    var faceContoursColor: Int = Color.WHITE

    // Blur face detection box
    var blurFaceDetectionBox: Boolean = false

    // Color encoding of the saved image.
    var colorEncoding: String = "RGB"
}
