package bgc.brasil.camera.models

data class OCRCNHResult(
    val name: String?,
    val renach: String?,
    val fatherName: String?,
    val identityCardNumber: String?,
    val motherName: String?,
    val birthDate: String?,
    val issuingAuthority: String?,
    val lastModificationDate: String?,
    val identityCardIssuingState: String?,
    val docEmissionPlace: String?,
    val registrationNumber: String?,
    val cpf: String?,
    val paidActivity: String?,
    val placeOfEmission: String?,
    val expirationDate: String?,
    val notes: String?,
    val firstLicenseDate: String?,
    val category: String?
)

data class OCRCNHSearch(
    val id: String,
    val type: String,
    val s3EncryptedLink: String,
    val result: OCRCNHResult
)
