package bgc.brasil.camera.models

data class OCRCNHResponse(
    val error: String?,
    val message: String,
    val search: OCRCNHSearch?
)
