package bgc.brasil.camera.controllers

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.ImageProxy
import androidx.core.content.ContextCompat
import bgc.brasil.camera.utils.jpegToBitmap

class PictureTakeController(
    private val context: Context
) {
    private var imageCapture: ImageCapture = ImageCapture
        .Builder()
        .setCaptureMode(ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY)
        .build()

    private fun makePictureCallback(handleImage: (image: Bitmap) -> Unit, handleError: (e: ImageCaptureException) -> Unit): ImageCapture.OnImageCapturedCallback {
        return object : ImageCapture.OnImageCapturedCallback() {
            @SuppressLint("UnsafeExperimentalUsageError", "UnsafeOptInUsageError")
            override fun onCaptureSuccess(imageProxy: ImageProxy) {
                val mediaImage = imageProxy.image ?: return

                val imageBitmap = mediaImage
                    .jpegToBitmap()

                handleImage(imageBitmap)
            }

            override fun onError(exception: ImageCaptureException) {
                handleError(exception)
            }
        }
    }

    fun getImageCapture(): ImageCapture {
        return this.imageCapture
    }

    fun takePicture(handleImage: (image: Bitmap) -> Unit, handleError: (e: ImageCaptureException) -> Unit) {
        this.imageCapture.takePicture(ContextCompat.getMainExecutor(this.context), makePictureCallback(handleImage, handleError))
    }

    companion object {
        private const val TAG = "PictureTakeController"
    }
}
