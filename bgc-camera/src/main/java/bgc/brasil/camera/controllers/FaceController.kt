/**
 * +-+-+-+-+-+-+
 * |y|o|o|n|i|t|
 * +-+-+-+-+-+-+
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Yoonit Camera lib for Android applications                      |
 * | Haroldo Teruya & Victor Goulart @ Cyberlabs AI 2020-2021        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

package bgc.brasil.camera.controllers

import android.Manifest
import android.content.Context
import androidx.camera.view.PreviewView
import bgc.brasil.camera.models.FaceParameters

/**
 * Class responsible to handle the camera operations.
 */
class FaceController(
    private val context: Context,
    private val previewView: PreviewView,
    private val graphicView: bgc.brasil.camera.CameraGraphicView,
    private val captureOptions: bgc.brasil.camera.models.CaptureOptions
) : CameraController(
    context,
    previewView,
    graphicView,
    captureOptions
) {
    // Called when a face is validates
    override fun onFaceValidated(params: FaceParameters) {
        this.cameraViewsEvents.onFaceValidated(params)
    }

    override fun onFaceFrame(isFaceDetected: Boolean) {
        this.cameraViewsEvents.onFrame(isFaceDetected)
    }

    companion object {
        private const val TAG = "FaceController"
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA) + arrayOf(Manifest.permission.INTERNET)
    }
}
