package bgc.brasil.camera.controllers

import android.content.Context
import android.graphics.Bitmap
import android.view.View
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCaptureException
import androidx.camera.view.PreviewView
import androidx.lifecycle.LifecycleOwner
import bgc.brasil.camera.enums.OCRApi
import bgc.brasil.camera.listeners.CameraCallbacks
import bgc.brasil.camera.models.DocumentParameters
import bgc.brasil.camera.requests.OCRRequest
import java.lang.AssertionError

/**
 * Class responsible to handle the camera operations.
 */
class DocumentController(
    private val context: Context,
    private val previewView: PreviewView,
    graphicView: bgc.brasil.camera.CameraGraphicView,
    private val captureOptions: bgc.brasil.camera.models.CaptureOptions
) : CameraController(
    context,
    previewView,
    graphicView,
    captureOptions
) {
    private var pictureTakeController: PictureTakeController = PictureTakeController(context)
    private var documentOk: Boolean = false
    val ocrRequest = OCRRequest(this as CameraCallbacks)
    // Called when a document is validated
    override fun onDocumentValidated(params: DocumentParameters) {
        this.cameraViewsEvents.onDocumentDetectedAndValidated(params)

        documentOk = params.valid
    }
    // Called when request finished with success.
    override fun onAnalyzerRequestSuccess() {
        documentOk = false

        super.onAnalyzerRequestSuccess()
    }

    override fun buildCameraPreview() {
        this.cameraProviderProcess?.unbindAll()

        this.previewView.visibility = View.VISIBLE

        val cameraSelector = CameraSelector
            .Builder()
            .requireLensFacing(captureOptions.cameraLens)
            .build()

        this.camera = this.cameraProviderProcess?.bindToLifecycle(
            this.context as LifecycleOwner,
            cameraSelector,
            this.imageAnalyzerController.analysis,
            this.preview,
            this.pictureTakeController.getImageCapture()
        )

        this.preview.setSurfaceProvider(this.previewView.surfaceProvider)

        this.startCaptureType()
    }

    fun takePicture() {
        if (documentOk) {
            this.cameraViewsEvents.onLoadingChange(true)
            this.cameraViewsEvents.startedCapture()
            pictureTakeController.takePicture(
                { handleDocImage(it) },
                { handleDocError(it) }
            )
        }
    }
    private fun handleDocImage(image: Bitmap) {
        when (captureOptions.ocrApi) {
            OCRApi.CNH -> ocrRequest.createCNHDocument(image)
            OCRApi.RG -> ocrRequest.createRGDocument(image)
            else -> throw AssertionError()
        }
    }
    private fun handleDocError(e: ImageCaptureException) {
        this.cameraViewsEvents.onLoadingChange(false)
        this.cameraViewsEvents.onFailTakePictureMessage(e.toString())
    }

    companion object {
        private const val TAG = "DocumentController"
    }
}
