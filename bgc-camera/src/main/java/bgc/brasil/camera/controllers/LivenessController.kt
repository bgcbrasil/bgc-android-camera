/**
 * +-+-+-+-+-+-+
 * |y|o|o|n|i|t|
 * +-+-+-+-+-+-+
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Yoonit Camera lib for Android applications                      |
 * | Haroldo Teruya & Victor Goulart @ Cyberlabs AI 2020-2021        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

package bgc.brasil.camera.controllers

import android.Manifest
import android.content.Context
import androidx.camera.view.PreviewView
import bgc.brasil.camera.enums.LivenessSteps
import bgc.brasil.camera.models.FaceParameters

/**
 * Class responsible to handle the camera operations.
 */
class LivenessController(
    private val context: Context,
    private val previewView: PreviewView,
    private val graphicView: bgc.brasil.camera.CameraGraphicView,
    private val captureOptions: bgc.brasil.camera.models.CaptureOptions
) : CameraController(
    context,
    previewView,
    graphicView,
    captureOptions
) {

    override fun onBox(inBox: Boolean) {
        this.cameraViewsEvents.onRenderBox(inBox)
    }

    override fun onLivenessChallengeChange(livenessStep: LivenessSteps) {
        this.cameraViewsEvents.onChallengeChange(livenessStep)
    }

    override fun onFaceValidated(params: FaceParameters) {
        this.cameraViewsEvents.onFaceValidated(params)
    }

    override fun onFaceFrame(isFaceDetected: Boolean) {
        this.cameraViewsEvents.onFrame(isFaceDetected)
    }

    companion object {
        private const val TAG = "LivenessController"
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA) + arrayOf(Manifest.permission.INTERNET)
    }
}
