/**
 * +-+-+-+-+-+-+
 * |y|o|o|n|i|t|
 * +-+-+-+-+-+-+
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Yoonit Camera lib for Android applications                      |
 * | Haroldo Teruya & Victor Goulart @ Cyberlabs AI 2020-2021        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

package bgc.brasil.camera.controllers

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.view.View
import androidx.camera.core.Camera
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import bgc.brasil.camera.analyzers.DocumentAnalyzer
import bgc.brasil.camera.analyzers.FaceAnalyzer
import bgc.brasil.camera.analyzers.FrameAnalyzer
import bgc.brasil.camera.analyzers.LivenessAnalyzer
import bgc.brasil.camera.analyzers.QRCodeAnalyzer
import bgc.brasil.camera.enums.CaptureType
import bgc.brasil.camera.enums.ImageRecognitionApi
import bgc.brasil.camera.enums.OCRApi
import bgc.brasil.camera.listeners.CameraCallbacks
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.listeners.CameraViewsEvents

/**
 * Class responsible to handle the camera operations.
 */
sealed class CameraController(
    private val context: Context,
    private val previewView: PreviewView,
    private val graphicView: bgc.brasil.camera.CameraGraphicView,
    private val captureOptions: bgc.brasil.camera.models.CaptureOptions
) : CameraCallbacks() {

    // Camera Event object - Events that our lib exposes
    var cameraEvents: CameraEvents = object : CameraEvents() { }

    // Camera Views Events - Events that alters our Views
    var cameraViewsEvents: CameraViewsEvents = object : CameraViewsEvents() { }

    // Image analyzer handle build, start and stop.
    protected var imageAnalyzerController = ImageAnalyzerController(this.graphicView)

    // Preview and ProcessCameraProvider both used to resume camera setup after toggle lens.
    protected lateinit var preview: Preview
    private var cameraProviderFuture = ProcessCameraProvider.getInstance(this.context)
    protected var cameraProviderProcess: ProcessCameraProvider? = null
    protected var camera: Camera? = null

    // Called when number of images reached.
    override fun onStopAnalyzer() {
        this.finishAnalyzer()
    }
    // Called when request started.
    override fun onAnalyzerRequestStarted() {
        this.cameraViewsEvents.onLoadingChange(true)
        this.stopAnalyzer()
    }

    // Called when request finished with success.
    override fun onAnalyzerRequestSuccess() {
        this.cameraViewsEvents.onSuccessMessage { cameraEvents.onSuccessDialogClose() }
        this.cameraViewsEvents.onLoadingChange(false)
        this.finishAnalyzer()
    }
    // Called when request finished with fail.
    override fun onAnalyzerRequestFail(errorCode: String) {
        this.cameraViewsEvents.onLoadingChange(false)

        if (captureOptions.imageRecognitionApi !== ImageRecognitionApi.NONE) {
            this.cameraViewsEvents.onFailFaceMessage(errorCode) { startCaptureType() }
        } else if (captureOptions.ocrApi !== OCRApi.NONE) {
            this.cameraViewsEvents.onFailOcrMessage(errorCode) { startCaptureType() }
        }
    }

    override fun onDebug(message: String) {
        this.cameraViewsEvents.onDebug(message)
    }

    init {
        this.imageAnalyzerController.build()
    }

    /**
     * Start camera preview if has permission.
     */
    fun startPreview() {
        // Emit permission denied if do not has permission.
        if (!this.isAllPermissionsGranted()) {
            this.cameraEvents.onPermissionDenied()
            return
        }

        this.cameraProviderFuture.addListener(
            Runnable {
                try {
                    this.cameraProviderProcess = cameraProviderFuture.get()
                    this.cameraProviderProcess?.unbindAll()

                    this.preview = Preview
                        .Builder()
                        .build()

                    this.buildCameraPreview()
                } catch (e: Exception) {
                    this.cameraEvents.onError(e.toString())
                }
            },
            ContextCompat.getMainExecutor(this.context)
        )
    }

    /**
     * Stop camera image analyzer and clear drawings.
     */
    fun finishAnalyzer() {
        this.imageAnalyzerController.stop()
    }

    /**
     * Stop camera image.
     */
    fun stopAnalyzer() {
        this.imageAnalyzerController.stop()
    }

    /**
     * - Camera provider process unbind all;
     * - Stop analyzers;
     * - Set capture type to NONE;
     * - Hide previewView;
     */
    fun destroy() {
        this.cameraProviderProcess?.unbindAll()

        this.stopAnalyzer()
        this.previewView.visibility = View.INVISIBLE
    }

    /**
     * Start image analyzer based on the capture type.
     */
    protected fun startCaptureType() {
        // If camera preview already is running, re-build camera preview.
        this.cameraProviderProcess?.let {
            this.imageAnalyzerController.stop()

            when (captureOptions.type) {
                CaptureType.FACE -> this.imageAnalyzerController.start(
                    FaceAnalyzer(
                        this.context,
                        this.cameraEvents,
                        this.graphicView,
                        this as CameraCallbacks,
                        captureOptions
                    )
                )

                CaptureType.QRCODE -> this.imageAnalyzerController.start(
                    QRCodeAnalyzer(
                        context,
                        this.cameraEvents,
                        this.graphicView,
                        captureOptions
                    )
                )

                CaptureType.FRAME -> this.imageAnalyzerController.start(
                    FrameAnalyzer(
                        this.context,
                        this.cameraEvents,
                        this.graphicView,
                        this as CameraCallbacks,
                        captureOptions
                    )
                )

                CaptureType.DOCUMENT -> this.imageAnalyzerController.start(
                    DocumentAnalyzer(
                        this.context,
                        this.cameraEvents,
                        this.graphicView,
                        this as CameraCallbacks,
                        captureOptions
                    )
                )

                CaptureType.LIVENESS -> this.imageAnalyzerController.start(
                    LivenessAnalyzer(
                        this.context,
                        this.cameraEvents,
                        this.graphicView,
                        this as CameraCallbacks,
                        captureOptions
                    )
                )

                CaptureType.NONE -> this.stopAnalyzer()
            }
        }
    }

    /**
     * Toggle between Front and Back Camera.
     */
    fun toggleCameraLens() {

        // Set camera lens.
        captureOptions.cameraLens =
            if (captureOptions.cameraLens == CameraSelector.LENS_FACING_FRONT)
                CameraSelector.LENS_FACING_BACK
            else CameraSelector.LENS_FACING_FRONT

        // If camera preview already is running, re-build camera preview.
        this.cameraProviderProcess?.let {
            this.buildCameraPreview()
        }
    }

    /**
     * Set to enable/disable the device torch. Available only to camera lens "back".
     */
    fun setTorch(enable: Boolean) {
        this.camera?.cameraControl?.enableTorch(enable)
    }

    override fun buildCameraPreview() {
        this.cameraProviderProcess?.unbindAll()

        this.previewView.visibility = View.VISIBLE

        val cameraSelector = CameraSelector
            .Builder()
            .requireLensFacing(captureOptions.cameraLens)
            .build()

        this.camera = this.cameraProviderProcess?.bindToLifecycle(
            this.context as LifecycleOwner,
            cameraSelector,
            this.imageAnalyzerController.analysis,
            this.preview
        )
        this.preview.setSurfaceProvider(this.previewView.surfaceProvider)

        this.startCaptureType()
    }

    /**
     * Return if has permission or not to use the camera.
     */
    private fun isAllPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(this.context, it) == PackageManager.PERMISSION_GRANTED
    }

    companion object {
//        private const val TAG = "CameraController"
        private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA) + arrayOf(Manifest.permission.INTERNET)
    }
}
