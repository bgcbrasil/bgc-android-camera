package bgc.brasil.camera

import ai.cyberlabs.yoonit.camera.R
import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.camera.view.PreviewView
import bgc.brasil.camera.controllers.LivenessController
import bgc.brasil.camera.enums.CaptureType
import bgc.brasil.camera.enums.LivenessSteps
import bgc.brasil.camera.graphics.LivenessRectangleView
import bgc.brasil.camera.listeners.CameraViewsEvents
import bgc.brasil.camera.models.LivenessStep

/**
 *  This class represents the camera liveness layout.
 */
open class LivenessView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : CameraView(context, attrs, defStyle, defStyleRes) {
    private var rectangleView: LivenessRectangleView

    /**
     * Inflate CameraView layout and instantiate.
     */
    init {

        LayoutInflater.from(context).inflate(
            R.layout.livenessview_layout,
            this,
            true
        )

        this.rectangleView = findViewById<bgc.brasil.camera.graphics.LivenessRectangleView>(R.id.liveness_FaceRectangleView)

        this.cameraController = LivenessController(
            context,
            findViewById<PreviewView>(R.id.previewView),
            findViewById<CameraGraphicView>(R.id.graphicView),
            captureOptions
        )

        this.cameraController.cameraViewsEvents = object : CameraViewsEvents() {
            override fun onRenderBox(isVisible: Boolean) {
                renderBox(isVisible)
            }

            @SuppressLint("SetTextI18n")
            override fun onChallengeChange(livenessStep: LivenessSteps) {
                findViewById<CameraGraphicView>(R.id.graphicView).visibility = if (livenessStep == LivenessSteps.BOX) View.VISIBLE else View.INVISIBLE
                when (livenessStep) {
                    LivenessSteps.BOX -> findViewById<TextView>(R.id.instructions).text = resources.getString(R.string.instructions_face_position)
                    LivenessSteps.LOOKUP -> findViewById<TextView>(R.id.instructions).text = resources.getString(R.string.instructions_face_look_up)
                    LivenessSteps.LOOKDOWN -> findViewById<TextView>(R.id.instructions).text = resources.getString(R.string.instructions_face_look_down)
                    LivenessSteps.LOOKRIGTH -> findViewById<TextView>(R.id.instructions).text = resources.getString(R.string.instructions_face_look_right)
                    LivenessSteps.LOOKLEFT -> findViewById<TextView>(R.id.instructions).text = resources.getString(R.string.instructions_face_look_left)
                    LivenessSteps.SMILE -> findViewById<TextView>(R.id.instructions).text = resources.getString(R.string.instructions_face_check_smile)
                    LivenessSteps.FINISHED -> findViewById<TextView>(R.id.instructions).text = resources.getString(R.string.instructions_face_finished)
                    else -> findViewById<TextView>(R.id.instructions).text = "Desafio inválido!"
                }
            }

            override fun onDebug(
                message: String
            ) {
                findViewById<TextView>(R.id.debugLiveness).text = message
            }
        }
    }

    /**
     * Start camera preview if has permission.
     */
    override fun startPreview() {
        captureOptions.type = CaptureType.LIVENESS
        this.setComputerVision(true)
        super.startPreview()
    }

    override fun setDebugMode(enable: Boolean) {
        findViewById<TextView>(R.id.debugLiveness).visibility = if (enable) View.VISIBLE else View.INVISIBLE
    }

    fun setSteps(steps: Array<String>) {
        val tempSteps = mutableListOf<LivenessStep>()
        var index = 0

        steps.reverse()
        steps.forEach {
            val selectedStep = when (it) {
                "lookUp" -> LivenessSteps.LOOKUP
                "lookDown" -> LivenessSteps.LOOKDOWN
                "lookLeft" -> LivenessSteps.LOOKLEFT
                "lookRight" -> LivenessSteps.LOOKRIGTH
                "smile" -> LivenessSteps.SMILE
                "box" -> LivenessSteps.BOX
                else -> LivenessSteps.FINISHED
            }
            val nextStep = if (index > 0) tempSteps[index - 1] else LivenessStep(LivenessSteps.FINISHED)

            tempSteps.add(LivenessStep(selectedStep, nextStep))
            index += 1
        }

        captureOptions.livenessStep = LivenessStep(LivenessSteps.NONE, tempSteps.last())
    }

    fun renderBox(isVisible: Boolean) {
        val box = findViewById<bgc.brasil.camera.graphics.LivenessRectangleView>(R.id.liveness_FaceRectangleView)

        if (isVisible) {
            this.setDetectionBox(true)
            box.visibility = View.VISIBLE
            return
        }

        this.setDetectionBox(false)
        box.visibility = View.INVISIBLE
    }

//    companion object {
//        private const val TAG = "LivenessView"
//    }
}
