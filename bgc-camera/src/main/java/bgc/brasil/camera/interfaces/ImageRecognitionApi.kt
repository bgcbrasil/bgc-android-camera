package bgc.brasil.camera.interfaces

import bgc.brasil.camera.models.CandidateFaceMatchRequest
import bgc.brasil.camera.models.CandidateFaceMatchResponse
import bgc.brasil.camera.models.CandidateFaceRequest
import bgc.brasil.camera.models.CandidateFaceResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface ImageRecognitionApi {
    @Headers("Content-Type: application/json")
    @POST("candidates-faces")
    fun createCandidateFace(@Body candidateFaceRequest: CandidateFaceRequest, @Header("Authorization") token: String): Call<CandidateFaceResponse>

    @Headers("Content-Type: application/json")
    @POST("candidates-faces-matches")
    fun createCandidateFaceMatch(@Body candidateFaceMatchRequest: CandidateFaceMatchRequest, @Header("Authorization") token: String): Call<CandidateFaceMatchResponse>
}
