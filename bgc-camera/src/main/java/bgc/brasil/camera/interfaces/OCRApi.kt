package bgc.brasil.camera.interfaces

import bgc.brasil.camera.models.OCRCNHResponse
import bgc.brasil.camera.models.OCRRGResponse
import bgc.brasil.camera.models.OCRRequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface OCRApi {
    @Headers("Content-Type: application/json")
    @POST("searches/bdc")
    fun createCNHSearch(@Body candidateFaceRequest: OCRRequestBody, @Header("Authorization") token: String): Call<OCRCNHResponse>

    @Headers("Content-Type: application/json")
    @POST("searches")
    fun createRGSearch(@Body candidateFaceRequest: OCRRequestBody, @Header("Authorization") token: String): Call<OCRRGResponse>
}
