package bgc.brasil.camera

import ai.cyberlabs.yoonit.camera.R
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.camera.view.PreviewView
import bgc.brasil.camera.controllers.DocumentController
import bgc.brasil.camera.enums.CaptureType
import bgc.brasil.camera.enums.DocumentParametersPositions
import bgc.brasil.camera.enums.OCRApi
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.listeners.CameraViewsEvents
import bgc.brasil.camera.models.DocumentFailedReasons
import bgc.brasil.camera.models.DocumentParameters
import bgc.brasil.camera.models.KeyError

class DocumentView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : CameraView(
    context,
    attrs,
    defStyle,
    defStyleRes
) {

    private var capturingAndAnalyzing: Boolean = false

    /**
     * Inflate CameraView layout and instantiate CameraController.
     */
    init {
        LayoutInflater.from(context).inflate(
            R.layout.documentview_layout,
            this,
            true
        )

        this.cameraController = DocumentController(
            context,
            findViewById<PreviewView>(R.id.previewView),
            findViewById<CameraGraphicView>(R.id.graphicView),
            captureOptions
        )

        this.cameraController.cameraViewsEvents = object : CameraViewsEvents() {

            override fun onLoadingChange(loading: Boolean) {
                if (loading) startLoading(findViewById<androidx.constraintlayout.widget.ConstraintLayout>(R.id.appBody))
                else stopLoading(findViewById<androidx.constraintlayout.widget.ConstraintLayout>(R.id.appBody))
            }

            override fun onSuccessMessage(onClose: () -> Unit) {
                isCapturingOrAnalyzing(false)

                when (captureOptions.ocrApi) {
                    OCRApi.RG -> messageView.onSuccessRgOcr { onClose() }
                    OCRApi.CNH -> messageView.onSuccessCnhOcr { onClose() }
                    else -> { /* nothing */ }
                }
            }

            override fun onDocumentDetectedAndValidated(params: DocumentParameters) {
                setButtonState(params.valid)
                setLightAlert(params.dark, params.bright)
                setDistanceAlert(params.distance)
            }

            override fun startedCapture() {
                isCapturingOrAnalyzing(true)
                setButtonState(false)
            }

            override fun onFailOcrMessage(errorCode: String, tryAgain: () -> Unit) {
                when (errorCode) {
                    DocumentFailedReasons.PoorQuality -> messageView.onMissingParametersError {
                        tryAgain()
                        isCapturingOrAnalyzing(false)
                        setButtonState(true)
                    }
                    else -> messageView.onUnexpectedError {
                        tryAgain()
                        isCapturingOrAnalyzing(false)
                        setButtonState(true)
                    } // Unexpected or request error
                }
            }

            override fun onFailTakePictureMessage(error: String) {
                messageView.onUnexpectedTakePictureError {
                    setButtonState(true)
                }
            }

            override fun onDebug(message: String) {
                findViewById<TextView>(R.id.debugFaceParameters).text = message
            }
        }

        this.setButton()
    }

    override fun startPreview() {
        captureOptions.type = CaptureType.DOCUMENT

        super.startPreview()
    }

    override fun setDebugMode(enable: Boolean) {
        findViewById<TextView>(R.id.debugFaceParameters).visibility = if (enable) View.VISIBLE else View.INVISIBLE
        this.setDetectionBox(enable)
    }

    private fun isCapturingOrAnalyzing(enable: Boolean) {
        capturingAndAnalyzing = enable
    }

    override fun setCameraEvents(cameraEvents: CameraEvents) {
        (cameraController as DocumentController).ocrRequest.cameraEvents = cameraEvents
        super.setCameraEvents(cameraEvents)
    }

    /**
     * Set to enable/disable the feature to make ocr on OCR service.
     *
     * @param enable The indicator to enable or disable.
     * Default value is false.
     * @param type 'rg' or 'cnh'
     */
    fun setDocumentOCR(enable: Boolean, type: String) {
        if (enable) {
            captureOptions.ocrApi = when (type) {
                "rg" -> OCRApi.RG
                "cnh" -> OCRApi.CNH
                else -> throw java.lang.IllegalArgumentException(KeyError.INVALID_OCR_PARAMS)
            }
        } else {
            captureOptions.ocrApi = OCRApi.NONE
        }
    }

    private fun setDistanceAlert(distance: DocumentParametersPositions) {
        when (distance) {
            DocumentParametersPositions.OK -> {
                findViewById<TextView>(R.id.instructions2_text).text = resources.getString(R.string.instructions_document_success_distance)
                findViewById<ImageView>(R.id.instructions2_icon).setImageResource(R.drawable.ic_check_icon)
            }

            DocumentParametersPositions.FAR -> {
                findViewById<TextView>(R.id.instructions2_text).text = resources.getString(R.string.instructions_document_fail_distance_far)
                findViewById<ImageView>(R.id.instructions2_icon).setImageResource(R.drawable.ic_error_icon)
            }

            DocumentParametersPositions.TOOCLOSE -> {
                findViewById<TextView>(R.id.instructions2_text).text = resources.getString(R.string.instructions_document_fail_distance_close)
                findViewById<ImageView>(R.id.instructions2_icon).setImageResource(R.drawable.ic_error_icon)
            }

            DocumentParametersPositions.NOTFOUND -> {
                findViewById<TextView>(R.id.instructions2_text).text =
                    resources.getString(R.string.instructions_document_fail_not_found)
                findViewById<ImageView>(R.id.instructions2_icon).setImageResource(R.drawable.ic_error_icon)
            }
            else -> {
                // nothing
            }
        }
    }

    private fun setLightAlert(dark: Boolean, bright: Boolean) {
        if (!(dark || bright)) {
            findViewById<TextView>(R.id.instructions1_text).text = resources.getString(R.string.instructions_document_success_light)
            findViewById<ImageView>(R.id.instructions1_icon).setImageResource(R.drawable.ic_check_icon)
        } else if (dark) {
            // dark message
            findViewById<TextView>(R.id.instructions1_text).text = resources.getString(R.string.instructions_document_fail_dark)
            findViewById<ImageView>(R.id.instructions1_icon).setImageResource(R.drawable.ic_error_icon)
        } else {
            // too bright message
            findViewById<TextView>(R.id.instructions1_text).text = resources.getString(R.string.instructions_document_fail_bright)
            findViewById<ImageView>(R.id.instructions1_icon).setImageResource(R.drawable.ic_error_icon)
        }
    }

    private fun setButton() {
        findViewById<Button>(R.id.mainButton).setOnClickListener {
            (this.cameraController as DocumentController).takePicture()
        }

        findViewById<Button>(R.id.mainButton).isEnabled = false
    }

    private fun setButtonState(enable: Boolean) {
        if (!capturingAndAnalyzing) findViewById<Button>(R.id.mainButton).isEnabled = enable
        else findViewById<Button>(R.id.mainButton).isEnabled = false
    }

    companion object {
        private const val TAG = "DocumentView"
    }
}
