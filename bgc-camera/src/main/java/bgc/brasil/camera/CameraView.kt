/**
 * +-+-+-+-+-+-+
 * |y|o|o|n|i|t|
 * +-+-+-+-+-+-+
 *
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Yoonit Camera lib for Android applications                      |
 * | Haroldo Teruya & Victor Goulart @ Cyberlabs AI 2020-2021        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

package bgc.brasil.camera

import ai.cyberlabs.yoonit.camera.R
import ai.cyberlabs.yoonit.camera.R.layout.loading_layout
import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.camera.core.CameraSelector
import androidx.constraintlayout.widget.ConstraintLayout
import bgc.brasil.camera.controllers.CameraController
import bgc.brasil.camera.controllers.PictureTakeController
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.models.CaptureOptions
import bgc.brasil.camera.models.KeyError
import bgc.brasil.camera.models.Message
import java.io.File
import kotlin.collections.ArrayList

/**
 * This class represents the camera layout.
 */
sealed class CameraView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(
    context,
    attrs,
    defStyle,
    defStyleRes
) {

    protected lateinit var pictureController: PictureTakeController
    // Camera controller object.
    protected lateinit var cameraController: CameraController

    // Camera interface event listeners object.
    private var cameraEvents: CameraEvents? = null

    var captureOptions: CaptureOptions = CaptureOptions

    private var isLoading: Boolean = false

    protected val messageView = MessageView(context)

    abstract fun setDebugMode(enable: Boolean)

    /**
     * Start camera preview if has permission.
     */
    open fun startPreview() {
        this.cameraController.startPreview()
    }

    /**
     * Stop camera image capture.
     */
    fun stopCapture() {
        this.cameraController.finishAnalyzer()
    }

    /**
     * Destroy camera preview.
     */
    fun destroy() {
        this.cameraController.destroy()
    }

    /**
     * Toggle between Front and Back Camera.
     */
    fun toggleCameraLens() {
        this.cameraController.toggleCameraLens()
    }

    /**
     * Set camera lens: "front" or "back".
     *
     * @param cameraLens "back" || "front"
     */
    fun setCameraLens(cameraLens: String) {
        if (cameraLens != "front" && cameraLens != "back") {
            throw IllegalArgumentException(KeyError.INVALID_CAMERA_LENS)
        }

        val cameraSelector: Int =
            if (cameraLens == "front") CameraSelector.LENS_FACING_FRONT
            else CameraSelector.LENS_FACING_BACK

        if (captureOptions.cameraLens != cameraSelector) {
            this.cameraController.toggleCameraLens()
        }
    }

    /**
     * Get current camera lens.
     *
     * @return "front" || "back".
     * Default value is "front".
     */
    fun getCameraLens(): String {
        return if (captureOptions.cameraLens == CameraSelector.LENS_FACING_FRONT) "front" else "back"
    }

    /**
     * Set number of face/frame file images to create.
     *
     * @param numberOfImages The number of images to create.
     */
    fun setNumberOfImages(numberOfImages: Int) {
        if (numberOfImages < 0) {
            throw IllegalArgumentException(KeyError.INVALID_NUMBER_OF_IMAGES)
        }

        captureOptions.numberOfImages = numberOfImages
    }

    /**
     * Set saving face/frame images time interval in milli seconds.
     *
     * @param timeBetweenImages The time in milli seconds.
     * Default value is 1000.
     */
    fun setTimeBetweenImages(timeBetweenImages: Long) {
        if (timeBetweenImages < 0) {
            throw IllegalArgumentException(KeyError.INVALID_TIME_BETWEEN_IMAGES)
        }

        captureOptions.timeBetweenImages = timeBetweenImages
    }

    /**
     * Set face image width to be created.
     *
     * @param width The file image width in pixels.
     * Default value is 200.
     */
    fun setOutputImageWidth(width: Int) {
        if (width <= 0) {
            throw IllegalArgumentException(KeyError.INVALID_OUTPUT_IMAGE_WIDTH)
        }

        captureOptions.imageOutputWidth = width
    }

    /**
     * Set face image height to be created.
     *
     * @param height The file image height in pixels.
     * Default value is 200.
     */
    fun setOutputImageHeight(height: Int) {
        if (height <= 0) {
            throw IllegalArgumentException(KeyError.INVALID_OUTPUT_IMAGE_HEIGHT)
        }

        captureOptions.imageOutputHeight = height
    }

    /**
     * Set to enable/disable save images when capturing face/frame.
     *
     * @param enable The indicator to enable or disable the face/frame save images.
     * Default value is false.
     */
    fun setSaveImageCaptured(enable: Boolean) {
        captureOptions.saveImageCaptured = enable
    }

    /**
     * Set token that will be used to call BGC's services.
     *
     * @param token Token passed to you by BGC's support.
     */
    fun setToken(token: String) {
        captureOptions.token = token
    }

    /**
     * Set to enable/disable detection box when face/qrcode detected.
     * The detection box is the the face/qrcode bounding box normalized to UI.
     *
     * @param enable: The indicator to enable/disable detection box.
     * Default value is `false`.
     */
    fun setDetectionBox(enable: Boolean) {
        captureOptions.detectionBox = enable
    }

    /**
     * Set detection box ARGB color.
     *
     * @param alpha The alpha value.
     * @param red The red value.
     * @param green The green value.
     * @param blue The blue value.
     * Default value is `(100, 255, 255, 255)`.
     */
    fun setDetectionBoxColor(
        alpha: Int,
        red: Int,
        green: Int,
        blue: Int
    ) {
        if (
            alpha < 0 || alpha > 255 ||
            red < 0 || red > 255 ||
            green < 0 || green > 255 ||
            blue < 0 || blue > 255
        ) {
            throw java.lang.IllegalArgumentException(KeyError.INVALID_DETECTION_BOX_COLOR)
        }

        captureOptions.detectionBoxColor = Color.argb(alpha, red, green, blue)
    }

    /**
     * Limit the minimum face capture size.
     * This variable is the face detection box percentage in relation with the UI graphic view.
     * The value must be between 0 and 1.
     *
     * For example, if set 0.5, will capture face with the detection box width occupying
     * at least 50% of the screen width.
     *
     * @param minimumSize The face capture min size value.
     * Default value is 0.0f.
     */
    fun setDetectionMinSize(minimumSize: Float) {
        if (minimumSize < 0.0f || minimumSize > 1.0f) {
            throw IllegalArgumentException(KeyError.INVALID_MINIMUM_SIZE)
        }

        captureOptions.minimumSize = minimumSize
    }

    /**
     * Limit the maximum face capture size.
     * This variable is the face detection box percentage in relation with the UI graphic view.
     * The value must be between 0 and 1.
     *
     * For example, if set 0.7, will capture face with the detection box width occupying
     * at least 70% of the screen width.
     *
     * @param maximumSize The face capture max size value.
     * Default value is 1.0f.
     */
    fun setDetectionMaxSize(maximumSize: Float) {
        if (maximumSize < 0.0f || maximumSize > 1.0f) {
            throw IllegalArgumentException(KeyError.INVALID_MAXIMUM_SIZE)
        }

        captureOptions.maximumSize = maximumSize
    }

    /**
     * Represents the percentage.
     * Positive value enlarges and negative value reduce the top side of the detection.
     * Use the `setDetectionBox` to have a visual result.
     *
     * Default value: `0.0f`
     */
    var detectionTopSize: Float = captureOptions.detectionTopSize
        set(value) {
            captureOptions.detectionTopSize = value
            field = value
        }

    /**
     * Represents the percentage.
     * Positive value enlarges and negative value reduce the right side of the detection.
     * Use the `setDetectionBox` to have a visual result.
     *
     * Default value: `0.0f`
     */
    var detectionRightSize: Float = captureOptions.detectionRightSize
        set(value) {
            captureOptions.detectionRightSize = value
            field = value
        }

    /**
     * Represents the percentage.
     * Positive value enlarges and negative value reduce the bottom side of the detection.
     * Use the `setDetectionBox` to have a visual result.
     *
     * Default value: `0.0f`
     */
    var detectionBottomSize: Float = captureOptions.detectionBottomSize
        set(value) {
            captureOptions.detectionBottomSize = value
            field = value
        }

    /**
     * Represents the percentage.
     * Positive value enlarges and negative value reduce the left side of the detection.
     * Use the `setDetectionBox` to have a visual result.
     *
     * Default value: `0.0f`
     */
    var detectionLeftSize: Float = captureOptions.detectionLeftSize
        set(value) {
            captureOptions.detectionLeftSize = value
            field = value
        }

    /**
     * Set to enable/disable face contours when face detected.
     *
     * @param enable The indicator to show or hide the face contours.
     * Default value is true.
     */
    fun setFaceContours(enable: Boolean) {
        captureOptions.faceContours = enable
    }

    /**
     * Set face contours ARGB color.
     *
     * @param alpha The alpha value.
     * @param red The red value.
     * @param green The green value.
     * @param blue The blue value.
     * Default value is `(100, 255, 255, 255)`.
     */
    fun setFaceContoursColor(
        alpha: Int,
        red: Int,
        green: Int,
        blue: Int
    ) {
        if (
            alpha < 0 || alpha > 255 ||
            red < 0 || red > 255 ||
            green < 0 || green > 255 ||
            blue < 0 || blue > 255
        ) {
            throw java.lang.IllegalArgumentException(KeyError.INVALID_FACE_CONTOURS_COLOR)
        }

        captureOptions.faceContoursColor = Color.argb(alpha, red, green, blue)
    }

    /**
     * Set to enable/disable the device torch. Available only to camera lens "back".
     *
     * @param enable The indicator to set enable/disable the device torch.
     * Default value is false.
     */
    fun setTorch(enable: Boolean) {
        if (captureOptions.cameraLens == CameraSelector.LENS_FACING_FRONT) {
            this.cameraEvents?.onMessage(Message.INVALID_TORCH_LENS_USAGE)
            return
        }

        this.cameraController.setTorch(enable)
    }

    /**
     * Set to apply enable/disable region of interest.
     *
     * @param enable The indicator to enable/disable region of interest.
     * Default value is `false`.
     */
    fun setROI(enable: Boolean) {
        captureOptions.roi.enable = enable
    }

    /**
     * Camera preview top distance in percentage.
     *
     * @param topOffset Value between `0` and `1`. Represents the percentage.
     * Default value is `0.0`.
     */
    fun setROITopOffset(topOffset: Float) {
        if (topOffset < 0.0f || topOffset > 1.0f) {
            throw IllegalArgumentException(KeyError.INVALID_ROI_TOP_OFFSET)
        }

        captureOptions.roi.topOffset = topOffset
    }

    /**
     * Camera preview right distance in percentage.
     *
     * @param rightOffset Value between `0` and `1`. Represents the percentage.
     * Default value is `0.0`.
     */
    fun setROIRightOffset(rightOffset: Float) {
        if (rightOffset < 0.0f || rightOffset > 1.0f) {
            throw IllegalArgumentException(KeyError.INVALID_ROI_RIGHT_OFFSET)
        }

        captureOptions.roi.rightOffset = rightOffset
    }

    /**
     * Camera preview bottom distance in percentage.
     *
     * @param bottomOffset Value between `0` and `1`. Represents the percentage.
     * Default value is `0.0`.
     */
    fun setROIBottomOffset(bottomOffset: Float) {
        if (bottomOffset < 0.0f || bottomOffset > 1.0f) {
            throw IllegalArgumentException(KeyError.INVALID_ROI_BOTTOM_OFFSET)
        }

        captureOptions.roi.bottomOffset = bottomOffset
    }

    /**
     * Camera preview left distance in percentage.
     *
     * @param leftOffset Value between `0` and `1`. Represents the percentage.
     * Default value is `0.0`.
     */
    fun setROILeftOffset(leftOffset: Float) {
        if (leftOffset < 0.0f || leftOffset > 1.0f) {
            throw IllegalArgumentException(KeyError.INVALID_ROI_LEFT_OFFSET)
        }

        captureOptions.roi.leftOffset = leftOffset
    }

    /**
     * Set to enable/disable region of interest offset visibility.
     *
     * @param enable The indicator to enable/disable region of interest visibility.
     * Default value is `false`.
     */
    fun setROIAreaOffset(enable: Boolean) {
        captureOptions.roi.areaOffsetEnable = enable
//        CameraView cant access graphicView at this moment
//        I didn't fixed this because we don't even use this method
//        this.graphicView.postInvalidate()
    }

    /**
     * Set face region of interest area offset color.
     *
     * @param alpha Integer that represents the alpha.
     * @param red Integer that represent red color.
     * @param green Integer that represent green color.
     * @param blue Integer that represent blue color.
     * Default value is 100, 255, 255, 255 (white color).
     */
    fun setROIAreaOffsetColor(
        alpha: Int,
        red: Int,
        green: Int,
        blue: Int
    ) {
        if (
            alpha < 0 || alpha > 255 ||
            red < 0 || red > 255 ||
            green < 0 || green > 255 ||
            blue < 0 || blue > 255
        ) {
            throw java.lang.IllegalArgumentException(KeyError.INVALID_ROI_COLOR)
        }

        captureOptions.roi.areaOffsetColor = Color.argb(alpha, red, green, blue)
//        CameraView cant access graphicView at this moment
//        I didn't fixed this because we don't even use this method
//        this.graphicView.postInvalidate()
    }

    /**
     * Enable/disable blur in face detection box.
     *
     * @param enable The indicator to enable/disable face detection box blur
     * Default value is `false`.
     */
    fun setBlurFaceDetectionBox(enable: Boolean) {
        captureOptions.blurFaceDetectionBox = enable
    }

    /**
     * Set the color encoding for the saved images.
     *
     * @param colorEncoding The color encoding type: "RGB" | "YUV".
     * Default value is `RGB`.
     */
    fun setColorEncodingCapture(colorEncoding: String) {
        if (colorEncoding != "RGB" && colorEncoding != "YUV") {
            throw IllegalArgumentException(KeyError.INVALID_IMAGE_CAPTURE_COLOR_ENCODING)
        }

        captureOptions.colorEncoding = colorEncoding
    }

    /**
     * Enable/disable computer vision usage.
     *
     * @param enable The indicator to enable/disable computer vision usage.
     * Default value is `false`.
     */
    fun setComputerVision(enable: Boolean) {
        captureOptions.computerVision.enable = enable
    }

    /**
     * Set the computer vision model paths to load.
     *
     * @param modelPaths The computer vision absolute model file path array list.
     * Default value is an empty array.
     */
    fun setComputerVisionLoadModels(modelPaths: ArrayList<String>) {
        modelPaths.forEach { modelPath ->
            if (!File(modelPath).exists()) {
                throw IllegalArgumentException("${KeyError.INVALID_COMPUTER_VISION_MODEL_PATHS}: $modelPath")
            }
        }

        captureOptions.computerVision.paths = modelPaths
    }

    /**
     * Clear loaded computer vision models.
     */
    fun computerVisionClearModels() {
        captureOptions.computerVision.clear()
    }

    open fun setCameraEvents(cameraEvents: CameraEvents) {
        this.cameraEvents = cameraEvents
        cameraController.cameraEvents = cameraEvents
    }

//    companion object {
//        private const val TAG = "CameraView"
//    }

    protected fun startLoading(appBody: ConstraintLayout) {
        if (!isLoading) {
            isLoading = true
            val inflater = LayoutInflater.from(context)
            appBody.addView(inflater.inflate(loading_layout, null), -1, -1)
        }
    }

    protected fun stopLoading(appBody: ConstraintLayout) {
        if (isLoading) {
            isLoading = false
            appBody.removeView(findViewById(R.id.loadingView))
        }
    }
}
