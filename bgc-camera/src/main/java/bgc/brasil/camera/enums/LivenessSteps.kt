package bgc.brasil.camera.enums

enum class LivenessSteps {
    NONE,
    BOX,
    LOOKUP,
    LOOKDOWN,
    LOOKRIGTH,
    LOOKLEFT,
    SMILE,
    FINISHED,
}
