package bgc.brasil.camera.enums

enum class ImageRecognitionApi {
    NONE,
    CREATE,
    MATCH,
}
