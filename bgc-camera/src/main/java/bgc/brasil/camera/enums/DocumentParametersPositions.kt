package bgc.brasil.camera.enums

enum class DocumentParametersPositions {
    FAR,
    OK,
    TOOCLOSE,
    NOTFOUND,
    PRESENT,
    NOTPRESENT
}
