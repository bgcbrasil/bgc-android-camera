package bgc.brasil.camera.enums

enum class ButtonDialogType {
    SUCCESS,
    TRYAGAIN
}
