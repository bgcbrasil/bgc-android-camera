package bgc.brasil.camera.enums

enum class FaceParametersPositions {
    OK,
    UP,
    DOWN,
    LEFT,
    RIGHT,
    NEAR,
    FAR
}
