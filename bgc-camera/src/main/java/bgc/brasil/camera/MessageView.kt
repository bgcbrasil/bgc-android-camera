package bgc.brasil.camera

import ai.cyberlabs.yoonit.camera.R
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import bgc.brasil.camera.enums.ButtonDialogType

class MessageView(
    private val context: Context
) {
    private lateinit var dialog: Dialog

    private fun dialogHelper(header: String, body: String, buttonType: ButtonDialogType, buttonAction: () -> Unit) {
        val inflater: LayoutInflater = LayoutInflater.from(context)

        val view: View = inflater.inflate(R.layout.dialog_layout, null)

        view.findViewById<TextView>(R.id.dialog_header).text = header
        view.findViewById<TextView>(R.id.dialog_body).text = body

        val button: Button = view.findViewById(R.id.dialog_button)

        when (buttonType) {
            ButtonDialogType.SUCCESS -> {
                button.text = context.getString(R.string.close_text)
            }
            ButtonDialogType.TRYAGAIN -> {
                button.text = context.getString(R.string.tryAgain_text)
            }
        }

        button.setOnClickListener {
            buttonAction()

            dialog.dismiss()
        }

        val builder = AlertDialog.Builder(this.context)
        builder.setView(view)
        builder.setCancelable(false)

        this.dialog = builder.create()
        dialog.show()
    }

    fun onSuccessFaceMatch(onClose: () -> Unit) {
        dialogHelper(
            context.getString(R.string.success_match_face_header),
            context.getString(R.string.success_match_face_body),
            ButtonDialogType.SUCCESS
        ) { onClose() }
    }

    fun onSuccessFaceCreate(onClose: () -> Unit) {
        dialogHelper(
            context.getString(R.string.success_create_face_header),
            context.getString(R.string.success_create_face_body),
            ButtonDialogType.SUCCESS
        ) { onClose() }
    }

    fun onFaceNotMatched(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.face_not_matched_header),
            context.getString(R.string.face_not_matched_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onUnexpectedError(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.unexpected_error_header),
            context.getString(R.string.unexpected_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun notFoundFaceId(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.unexpected_error_header),
            context.getString(R.string.unexpected_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onTooManyFaces(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.tooManyFaces_error_header),
            context.getString(R.string.tooManyFaces_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onMouthOpened(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.openedMouth_error_header),
            context.getString(R.string.openedMouth_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onWearingGlasses(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.wearingGlasses_error_header),
            context.getString(R.string.wearingGlasses_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onPoorQuality(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.poorQuality_error_header),
            context.getString(R.string.poorQuality_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onClosedEyes(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.closedEyes_error_header),
            context.getString(R.string.closedEyes_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onNoFaceRequest(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.noFace_error_header),
            context.getString(R.string.noFace_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onSmiling(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.smiling_error_header),
            context.getString(R.string.smiling_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onSuccessCnhOcr(onClose: () -> Unit) {
        dialogHelper(
            context.getString(R.string.success_rg_ocr_header),
            context.getString(R.string.success_rg_ocr_body),
            ButtonDialogType.SUCCESS
        ) { onClose() }
    }

    fun onSuccessRgOcr(onClose: () -> Unit) {
        dialogHelper(
            context.getString(R.string.success_cnh_ocr_header),
            context.getString(R.string.success_cnh_ocr_body),
            ButtonDialogType.SUCCESS
        ) { onClose() }
    }

    fun onMissingParametersError(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.missing_parameters_error_header),
            context.getString(R.string.missing_parameters_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }

    fun onUnexpectedTakePictureError(tryAgain: () -> Unit) {
        dialogHelper(
            context.getString(R.string.picture_take_error_header),
            context.getString(R.string.picture_take_error_body),
            ButtonDialogType.TRYAGAIN
        ) { tryAgain() }
    }
}
