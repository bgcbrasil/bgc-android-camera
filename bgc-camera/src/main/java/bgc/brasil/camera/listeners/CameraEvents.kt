package bgc.brasil.camera.listeners

import bgc.brasil.camera.models.CandidateFaceMatchResult
import bgc.brasil.camera.models.CandidateFaceResult
import bgc.brasil.camera.models.OCRCNHResult
import bgc.brasil.camera.models.OCRRGResult

abstract class CameraEvents {
    // Default Events
    open fun onImageCaptured(
        type: String,
        count: Int,
        total: Int,
        imagePath: String,
        inferences: ArrayList<android.util.Pair<String, FloatArray>>
    ) {}

    open fun onEndCapture() {}

    open fun onError(
        error: String
    ) {}

    open fun onMessage(
        message: String
    ) {}

    open fun onPermissionDenied() {}

    open fun onSuccessDialogClose() {}

    // End Default Events

    // FaceEvents
    open fun onFaceDetected(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        leftEyeOpenProbability: Float?,
        rightEyeOpenProbability: Float?,
        smilingProbability: Float?,
        headEulerAngleX: Float,
        headEulerAngleY: Float,
        headEulerAngleZ: Float
    ) {}

    open fun onFaceUndetected() {}

    open fun onFaceCreated(
        id: String?,
        result: CandidateFaceResult?
    ) {}

    open fun onFaceCreationFail(
        error: String?
    ) {}

    open fun onFaceMatch(
        id: String?,
        result: CandidateFaceMatchResult?
    ) {}

    open fun onFaceUnmatch(
        id: String?,
        result: CandidateFaceMatchResult?
    ) {}

    open fun onFaceMatchFail(
        error: String?
    ) {}

    // End FaceEvents

    // DocumentEvents
    open fun onCNHCreationFail(
        error: String?
    ) {}

    open fun onRGCreationFail(
        error: String?
    ) {}

    open fun onRGCreated(
        id: String?,
        result: OCRRGResult?
    ) {}

    open fun onCNHCreated(
        id: String?,
        result: OCRCNHResult?
    ) {}

    // End DocumentEvents

    // QRCode Events
    open fun onQRCodeScanned(
        content: String
    ) {}

    // End QRCODE Events

    // LivenessEvents
    open fun onLiveness(
        liveness: Boolean
    ) {}
    // End LivenessEvents
}
