package bgc.brasil.camera.listeners

import bgc.brasil.camera.enums.LivenessSteps
import bgc.brasil.camera.models.DocumentParameters
import bgc.brasil.camera.models.FaceParameters

abstract class CameraViewsEvents {
    open fun onLoadingChange(
        loading: Boolean = false
    ) {}

    open fun onSuccessMessage(
        onClose: () -> Unit
    ) {}

    open fun onFailFaceMessage(
        errorCode: String,
        tryAgain: () -> Unit
    ) {}

    open fun onFailOcrMessage(
        errorCode: String,
        tryAgain: () -> Unit
    ) {}

    open fun onFailTakePictureMessage(
        error: String
    ) {}

    open fun onFaceValidated(
        params: FaceParameters
    ) {}

    open fun onDocumentDetectedAndValidated(
        params: DocumentParameters
    ) {}

    open fun onFrame(
        isFaceDetected: Boolean
    ) {}

    open fun onRenderBox(
        isVisible: Boolean
    ) {}

    open fun onChallengeChange(
        livenessStep: LivenessSteps
    ) {}

    open fun onDebug(
        message: String
    ) {}

    open fun startedCapture() {}
}
