package bgc.brasil.camera.listeners

import bgc.brasil.camera.enums.LivenessSteps
import bgc.brasil.camera.models.DocumentParameters
import bgc.brasil.camera.models.FaceParameters

abstract class CameraCallbacks {
    abstract fun onStopAnalyzer()

    abstract fun onAnalyzerRequestStarted()

    abstract fun onAnalyzerRequestSuccess()

    abstract fun onAnalyzerRequestFail(
        errorCode: String
    )

    abstract fun onDebug(
        message: String
    )

    open fun onFaceValidated(
        params: FaceParameters
    ) {}

    open fun onDocumentValidated(
        params: DocumentParameters
    ) {}

    open fun onBox(
        inBox: Boolean
    ) {}

    open fun onFaceFrame(
        isFaceDetected: Boolean
    ) {}

    open fun onLivenessChallengeChange(
        livenessStep: LivenessSteps
    ) {}

    open fun buildCameraPreview() {}
}
