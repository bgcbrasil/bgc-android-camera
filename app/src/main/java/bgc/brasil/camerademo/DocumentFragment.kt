package bgc.brasil.camerademo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import bgc.brasil.camera.DocumentView
import bgc.brasil.camerademo.utils.buildCameraEventListener

class DocumentFragment : Fragment() {
    private lateinit var documentView: DocumentView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_document, container, false)

        documentView = view.findViewById(R.id.document_view)
        documentView.setCameraEvents(buildCameraEventListener(TAG, requireContext()))

        startCapture()

        return view
    }

    private fun startCapture() {
        documentView.setToken("TokenBGC().getToken()")
        documentView.setCameraLens("back") // back or front
        documentView.setDocumentOCR(true, "cnh")
        documentView.setDebugMode(true)
        documentView.startPreview()
    }

    companion object {
        private const val TAG = "DocumentFragment"
    }
}
