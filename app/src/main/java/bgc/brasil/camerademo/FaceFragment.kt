package bgc.brasil.camerademo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import bgc.brasil.camera.FaceView
import bgc.brasil.camerademo.utils.buildCameraEventListener

/**
 * TODO: Change name face create (fragment, layout, navigation)
 */
class FaceFragment : Fragment() {
    private lateinit var faceView: FaceView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_face, container, false)

        faceView = view.findViewById(R.id.camera_view)
        faceView.setCameraEvents(buildCameraEventListener(TAG, requireContext()))

        startCapture()
        return view
    }

    private fun startCapture() {
        faceView.setToken("TokenBGC().getToken()")
        faceView.setCameraLens("front") // back or front
        faceView.setDebugMode(false)
        faceView.setCreateFaceOnDetect(true)
        faceView.startPreview()
    }
    companion object {
        private const val TAG = "FaceFragment"
    }
}
