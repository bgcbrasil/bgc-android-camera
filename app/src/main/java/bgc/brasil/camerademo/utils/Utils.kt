package bgc.brasil.camerademo.utils

import android.app.Activity
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import android.util.Log
import android.util.Pair
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import bgc.brasil.camera.listeners.CameraEvents
import bgc.brasil.camera.models.CandidateFaceMatchResult
import bgc.brasil.camera.models.CandidateFaceResult
import bgc.brasil.camera.models.OCRCNHResult
import bgc.brasil.camera.models.OCRRGResult
import android.content.ClipData
import android.widget.Toast
import bgc.brasil.camerademo.R

fun checkPermissions(requiredPermissions: Array<String>, context: Context): Boolean {
    return requiredPermissions.all {
        ContextCompat.checkSelfPermission(
            context, it
        ) == PackageManager.PERMISSION_GRANTED
    }
}

fun hideKeyboard(activity: Activity) {
    val inputMethodManager =
        activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    // Check if no view has focus
    val currentFocusedView = activity.currentFocus
    currentFocusedView?.let {
        inputMethodManager.hideSoftInputFromWindow(
            currentFocusedView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS
        )
    }
}

fun buildCameraEventListener(tag: String, context: Context): CameraEvents = object : CameraEvents() {
    override fun onImageCaptured(
        type: String,
        count: Int,
        total: Int,
        imagePath: String,
        inferences: ArrayList<Pair<String, FloatArray>>
    ) {
        Log.d(tag, "onImageCaptured: $type")
    }

    override fun onSuccessDialogClose() {
        Log.d(tag, "Dialog box closed!")
    }

    override fun onLiveness(liveness: Boolean) {
        Log.d(tag, "Liveness $liveness")
    }

    override fun onFaceDetected(
        x: Int,
        y: Int,
        width: Int,
        height: Int,
        leftEyeOpenProbability: Float?,
        rightEyeOpenProbability: Float?,
        smilingProbability: Float?,
        headEulerAngleX: Float,
        headEulerAngleY: Float,
        headEulerAngleZ: Float
    ) {
        Log.d(tag, "onFaceDetected")
    }

    override fun onFaceCreated(
        id: String?,
        result: CandidateFaceResult?
    ) {
        Log.d(tag, "onFaceCreated: $id, $result")

        val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData: ClipData = ClipData.newPlainText("id", id)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(context, R.string.on_face_created, Toast.LENGTH_LONG).show()
    }

    override fun onFaceCreationFail(
        error: String?
    ) {
        Log.d(tag, "onFaceCreationFail: $error")
    }

    override fun onFaceMatch(id: String?, result: CandidateFaceMatchResult?) {
        Log.d(tag, "onFaceMatch: $id, $result")
    }

    override fun onFaceUnmatch(id: String?, result: CandidateFaceMatchResult?) {
        Log.d(tag, "onFaceUnmatch: $id, $result")
    }

    override fun onFaceMatchFail(error: String?) {
        Log.d(tag, "onFaceMatchCreationFail: $error")
    }

    override fun onFaceUndetected() {
        Log.d(tag, "onFaceUndetected")
    }

    override fun onEndCapture() {
        Log.d(tag, "onEndCapture")
    }

    override fun onCNHCreated(id: String?, result: OCRCNHResult?) {
        Log.d(tag, "onCNHCreated: $id, $result")
    }

    override fun onRGCreated(id: String?, result: OCRRGResult?) {
        Log.d(tag, "onRGCreated: $id, $result")
    }

    override fun onCNHCreationFail(error: String?) {
        Log.d(tag, "onCNHCreationFail: $error")
    }

    override fun onRGCreationFail(error: String?) {
        Log.d(tag, "onRGCreationFail: $error")
    }

    override fun onError(error: String) {
        Log.d(tag, "onError: $error")
    }

    override fun onMessage(message: String) {
        Log.d(tag, "onMessage: $message")
    }

    override fun onPermissionDenied() {
        Log.d(tag, "onPermissionDenied")
    }

    override fun onQRCodeScanned(content: String) {
        Log.d(tag, "onQRCodeScanned")
    }
}
