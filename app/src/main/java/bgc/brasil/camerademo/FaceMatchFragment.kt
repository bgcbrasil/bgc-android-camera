package bgc.brasil.camerademo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import bgc.brasil.camera.FaceView
import bgc.brasil.camerademo.utils.buildCameraEventListener
import java.lang.AssertionError

class FaceMatchFragment : Fragment() {
    private lateinit var faceView: FaceView
    private var faceMatchId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        faceMatchId = arguments?.getString("faceMatchId")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_face, container, false)

        faceView = view.findViewById(R.id.camera_view)
        faceView.setCameraEvents(buildCameraEventListener(TAG, requireContext()))

        startCapture()

        return view
    }

    private fun startCapture() {
        /**
         * TODO: Add SafeArgs
         */
        if (faceMatchId == null) {
            throw AssertionError("Failed to fetch face match id")
        }
        faceView.setToken("TokenBGC().getToken()")
        faceView.setCameraLens("front") // back or front
        faceView.setDebugMode(false)
        faceView.setFaceMatchOnDetect(true, faceMatchId!!)
        faceView.startPreview()
    }

    companion object {
        private const val TAG = "FaceFragment"
    }
}
