package bgc.brasil.camerademo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import bgc.brasil.camera.LivenessView
import bgc.brasil.camerademo.utils.buildCameraEventListener

class LivenessFragment : Fragment() {
    private lateinit var livenessView: LivenessView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_liveness, container, false)
        livenessView = view.findViewById(R.id.liveness_view)
        livenessView.setCameraEvents(buildCameraEventListener(TAG, requireContext()))

        livenessView.setDebugMode(true)
        livenessView.setCameraLens("front") // back or front
        livenessView.setSteps(arrayOf("box", "lookUp", "box", "lookDown", "smile"))
        livenessView.startPreview()

        return view
    }

    companion object {
        private const val TAG = "LivenessFragment"
    }
}
