package bgc.brasil.camerademo

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import bgc.brasil.camerademo.databinding.FragmentFaceMatchInitialBinding
import bgc.brasil.camerademo.utils.hideKeyboard

class FaceMatchInitialFragment : Fragment() {
    private var faceMatchId = ""

    private var _binding: FragmentFaceMatchInitialBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFaceMatchInitialBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val faceMatchInput = binding.faceMatchInput
        val faceMatchButton = binding.faceMatchNext

        // EditText onText listener
        faceMatchInput.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length == 36) {
                    faceMatchId = s.toString()
                    setButtonEnabled(true)
                    hideKeyboard(activity as MainActivity)
                    return
                }
                setButtonEnabled(false)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }

            override fun afterTextChanged(s: Editable?) { }
        })

        // Button on click listener
        faceMatchButton.setOnClickListener {
            val args = Bundle()
            args.putString("faceMatchId", faceMatchId)

            hideKeyboard(activity as MainActivity)

            findNavController().navigate(R.id.next_action, args)
        }
    }

    fun setButtonEnabled(enabled: Boolean) {
        binding.faceMatchNext.isEnabled = enabled
        binding.faceMatchNext.isClickable = enabled
        binding.faceMatchNext.isFocusable = enabled
    }

//    companion object {
//        private const val TAG = "FaceFragment"
//    }
}
